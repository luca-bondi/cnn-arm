# Authors: 
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#
#
# Slightly based on Nicolas Pinto's
# http://github.com/npinto/sclas/blob/master/svm_ova_fromfilenames.py
#
# License: BSD

import numpy as np
from sklearn.svm import SVC

DEFAULT_REGULARIZATION = 1e5
DEFAULT_TRACE_NORMALIZATION = True

def train_test(kernel_train, kernel_test, train_labels, cat, C):
    ltrain = np.zeros(kernel_train.shape[0])
    ltrain[train_labels != cat] = -1
    ltrain[train_labels == cat] = +1

    svm = SVC(kernel='precomputed', C=C, tol=1e-5)
    svm.fit(kernel_train, ltrain)
    resps = svm.decision_function(kernel_test)[:, 0]
    
    return resps

def svm_ova(train_set,
            test_set,
            # --
            train_labels,
            test_labels,
            # --
            C=DEFAULT_REGULARIZATION,
            trace_normalization=DEFAULT_TRACE_NORMALIZATION,
            ):
    
    
    multithread = False
    try:
        import multiprocessing
        if multiprocessing.cpu_count() > 1:
            from joblib import Parallel, delayed
            multithread = True
            #print 'Multithreading accuracy'
    except ImportError:
        print('No multi-threading due to library support missing')
    
    # SVM is up to 30 times faster with float32 than with int
    train_set = train_set.astype(np.float32)
    test_set = test_set.astype(np.float32)
    
    categories = np.unique(train_labels)
    n_categories = len(categories)
    n_test = test_set.shape[0]

    test_predictions = np.empty((n_test, n_categories))

    assert(test_set.shape[1] == train_set.shape[1])

    #print 'kernelizing features...'
    kernel_train = np.dot(train_set, train_set.T)
    kernel_test = np.dot(test_set, train_set.T)
    
    
    if trace_normalization:
        kernel_trace = kernel_train.trace()
        kernel_train = kernel_train / kernel_trace
        kernel_test = kernel_test / kernel_trace

    cat_index = {}
    for icat, cat in enumerate(categories):
        cat_index[cat] = icat
        
    # -- iterates over different categories.
    if multithread:
        par = Parallel(n_jobs=multiprocessing.cpu_count(),backend="threading")
        results = par(delayed(train_test)(
                                          kernel_train, kernel_test,
                                          train_labels,
                                          cat, C)
                      for cat in categories
                      )
        for res_idx,res in enumerate(results):
            test_predictions[:, res_idx] = res
    else:
        for icat, cat in enumerate(categories):
            test_predictions[:, icat]= \
                train_test(kernel_train, kernel_test,
                       train_labels, cat, C)

    gt = np.array([cat_index[e] for e in test_labels]).astype('int')

    pred = test_predictions.argmax(1)
    accuracy = 100. * (pred == gt).sum() / n_test

    return accuracy
