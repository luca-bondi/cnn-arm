#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
- Features and data loading
- SVM train and test with 10 fold cross validation
- Optional PCA and Quantization
"""
import argparse
import os
import time
import numexpr as ne

import functions as fct
import numpy as np
from svm_mt import svm_ova
from sklearn.decomposition import PCA

ACC_PATH = 'results/atc/accuracy/'
PCA_PATH = 'results/atc/features/pca/'

def rate_arithmetic_coding(train_set,test_set,symbols):
    '''
    Feature by feature
    '''
    
    assert train_set.dtype == np.int32
    assert test_set.dtype == np.int32
    assert symbols.dtype == np.int32
    
    # necessary for bincount
    assert train_set.min() >= 0
    assert test_set.min() >= 0
    assert symbols.min() >= 0
    
    rates = np.empty(train_set.shape[1])
    
    for feat_idx in range(train_set.shape[1]):
        
        train_set_feat = train_set[:,feat_idx]
        test_set_feat = test_set[:,feat_idx]
    
        # a priori probability for each symbol ---
        p_train = np.bincount(train_set_feat, minlength=symbols.size).astype(np.float32)
        
        # fix for symbols not in train_set
        p_train[p_train == 0.] = 1.
        p_train /= p_train.sum()
        #assert np.allclose(p_train.sum(),1.)
        
        # a posteriori probability for each symbol ---
        p_test = np.bincount(test_set_feat, minlength=symbols.size).astype(np.float32)
        p_test /= p_test.sum()
        #assert np.allclose(p_test.sum(),1.)
        
        # rate for the feature ---
        rates[feat_idx] = -np.sum(p_test*np.log2(p_train))
    
    return rates.sum()

def snr_measure(arr_original,arr_quantized):
    '''
    Zero mean arr needed as input
    '''
    assert arr_original.shape == arr_quantized.shape
    assert arr_original.dtype == arr_quantized.dtype
    assert arr_original.dtype == np.float32

    distortion = ne.evaluate('sum((arr_original-arr_quantized)**2)')
    signal_power = ne.evaluate('sum(arr_original**2)')
    snr = 10*np.log10(signal_power/distortion)
    return snr

def midtread_quantizer(train_set,test_set,bin_width,central_bin_width = None):
    '''
    Input data must be unbiased
    '''
    internal_time = False

    bin_width = np.float32(bin_width)
    
    if internal_time: t0 = time.time()
    # determine thresholds and centroids ---
    if central_bin_width is None: central_bin_width = bin_width
    
    abs_max = np.max([-train_set.min(),train_set.max()])
    
    th = np.arange(central_bin_width/2,abs_max,bin_width,dtype=np.float32)
    centr = np.arange(central_bin_width/2+bin_width/2,abs_max+bin_width/2,bin_width,dtype=np.float32)
    
    th = np.concatenate((-th[::-1],th))
    centr = np.concatenate((-centr[::-1],np.array([0]),centr))
    
    assert th.size == centr.size - 1
    
    symbols = np.arange(centr.size,dtype=np.int32)
    
    if internal_time:
        t1 = time.time()
        print('- Thresholds and centroids time: ' + str(t1-t0))
    
    # quantize with symbols ---
    train_set_q_s = np.zeros(train_set.shape,dtype=np.int32)
    test_set_q_s = np.zeros(test_set.shape,dtype=np.int32)
    
    for idx in range(1,th.size):
        th_min = th[idx-1]
        th_max = th[idx]
        sym = symbols[idx]
        
        train_set_q_s = ne.evaluate('where((train_set>=th_min)&(train_set<th_max),sym,train_set_q_s)')
        test_set_q_s = ne.evaluate('where((test_set>=th_min)&(test_set<th_max),sym,test_set_q_s)')
        
    # last level
    th_min = th[-1]
    sym = symbols[-1]
    
    train_set_q_s = ne.evaluate('where((train_set>=th_min),sym,train_set_q_s)')
    test_set_q_s = ne.evaluate('where((test_set>=th_min),sym,test_set_q_s)')
    
    if internal_time:
        t2 = time.time()
        print('- Quantization time: ' + str(t2-t1))

    # arithmetic coding rate ---

    rate = rate_arithmetic_coding(train_set_q_s,test_set_q_s,symbols)

    if internal_time:
        t3 = time.time()
        print('- Rate estimation time: ' + str(t3-t2))

    # symbols to centroids ---
    bias = np.int32((symbols.size-1)/2)

    train_set_q_c = ne.evaluate('(train_set_q_s-bias)*bin_width')
    test_set_q_c = ne.evaluate('(test_set_q_s-bias)*bin_width')

    train_set_q_s = None
    test_set_q_s = None

    if internal_time:
        t4 = time.time()
        print('- Symbols to centroids time: ' + str(t4-t3))

    dist_train = snr_measure(train_set,train_set_q_c)
    dist_test = snr_measure(test_set,test_set_q_c)
    
    if internal_time:
        t5 = time.time()
        print('- Distortion evaluation time: ' + str(t5-t4))
    
    #return train_set_q_s,test_set_q_s,train_set_q_c,test_set_q_c,symbols,centr,th
    return train_set_q_c, test_set_q_c, symbols, rate, dist_train, dist_test

def split_accuracy(
                  train_set,test_set,
                  train_labels,test_labels,
                  pca_comp_list=None,qstep_list=None,
                  split_idx=None,features_prefix=None):
    """
    Accuracy of a split
    @param train_set
    @param test_set
    @param train_labels
    @param test_labels
    @param pca_comp_list PCA number of components list (None = no PCA)
    @param qstep_list Quantizer step list (None = no quantization)
    @param split_idx for ETA estimation
    @param split_num for ETA estimation
    @param t0 for ETA estimation
    @return accuracy of the split np.float32
    """
    
    internal_time = False
    
    if qstep_list is None:
        qstep_list = [0,]
    
    # if needed calculate PCA with the maximum number of components
    if pca_comp_list is None:
        pca_comp_list = [0,]
    
    if max(pca_comp_list) > 0:
        train_set_reduced_max = None
        test_set_reduced_max = None
        pca_save = False
        if features_prefix is not None and split_idx is not None:
            pca_save = True
            pca_filepath = os.path.join(PCA_PATH,features_prefix+ '_')
            pca_train_filepath = pca_filepath + 'train-' + str(split_idx) + '.npy'
            pca_test_filepath = pca_filepath + 'test-' + str(split_idx) + '.npy'
            # load 
            if os.path.isfile(pca_train_filepath):
                train_set_reduced_max = np.load(pca_train_filepath)
                if os.path.isfile(pca_test_filepath):
                    test_set_reduced_max = np.load(pca_test_filepath)
                    if train_set_reduced_max.shape[1] == test_set_reduced_max.shape[1] and \
                    train_set_reduced_max.shape[1] >= max(pca_comp_list):
                        if split_idx is not None: print("Split %d - PCA loading"%(split_idx))
                        #Valid saved PCA
                        train_set_reduced_max = train_set_reduced_max[:,:max(pca_comp_list)]
                        test_set_reduced_max = test_set_reduced_max[:,:max(pca_comp_list)]
                        pca_save = False
                    else:
                        train_set_reduced_max = None
                        test_set_reduced_max = None
                    
        if train_set_reduced_max is None or test_set_reduced_max is None:
            # calculate PCA
            if max(pca_comp_list) > train_set.shape[0]:
                exit('Cannot compute ' + str(max(pca_comp_list)) + ' PCA components with ' + str(train_set.shape[0]) + ' training samples')
            if max(pca_comp_list) > train_set.shape[1]:
                exit('Cannot compute ' + str(max(pca_comp_list)) + ' PCA components form ' + str(train_set.shape[1]) + ' features')
            if split_idx is not None: print("Split %d - PCA calculating "%(split_idx) + str(train_set.shape))
            pca = PCA(); #calculate with the maximum number of components
            train_set_reduced_max = pca.fit_transform(train_set).astype(np.float32)
            test_set_reduced_max = pca.transform(test_set).astype(np.float32)
            if pca_save:
                if split_idx is not None: print("Split %d - PCA saving "%(split_idx))
                print(pca_train_filepath)
                np.save(pca_train_filepath,train_set_reduced_max)
                print(pca_test_filepath)
                np.save(pca_test_filepath,test_set_reduced_max)
        
        assert train_set_reduced_max.dtype==np.float32
        assert test_set_reduced_max.dtype==np.float32
    
    param_comb_idx = 0
    accs = np.empty(len(pca_comp_list)*len(qstep_list),dtype=np.float32)
    levels = np.empty(len(pca_comp_list)*len(qstep_list),dtype=np.float32)
    rate = np.empty(len(pca_comp_list)*len(qstep_list),dtype=np.float32)
    snr = np.empty((len(pca_comp_list)*len(qstep_list),2),dtype=np.float32) #snr[:,0] train, snr[:,1] test
    
    for pca_comp in pca_comp_list:
        
        for qstep in qstep_list:
            
            if internal_time: t0_comb = time.time()
            
            if pca_comp == 0:
                train_set_comb = train_set.copy()
                test_set_comb = test_set.copy()
                
                fct.featuresNormalize(train_set_comb, test_set_comb)
            
            else:
                train_set_comb = train_set_reduced_max[:,:pca_comp].copy()
                test_set_comb = test_set_reduced_max[:,:pca_comp].copy()
                
                fct.featuresUnbias(train_set_comb, test_set_comb)
            
            num_feat = train_set_comb.shape[1]
            
            if internal_time:
                t1_comb = time.time()
                print('Features normalization / unbias time: ' + str(t1_comb-t0_comb))
            
            if qstep == 0:
                snr[param_comb_idx,:] = 0
                levels[param_comb_idx] = 0
                rate[param_comb_idx] = 32 * num_feat
                
            else:
                # quantizer ---
                train_set_comb, test_set_comb,\
                quantized_levels,\
                rate[param_comb_idx],\
                snr[param_comb_idx,0], snr[param_comb_idx,1] = \
                    midtread_quantizer(train_set_comb, test_set_comb, qstep)

                levels[param_comb_idx] = len(quantized_levels)

                if internal_time:
                    t2_comb = time.time()
                    print('Features quantization time: ' + str(t2_comb-t1_comb))
                    
            if internal_time: t5_comb = time.time()
            accs[param_comb_idx] = svm_ova(train_set_comb,
                                           test_set_comb,
                                           train_labels,test_labels)
            if internal_time:
                t6_comb = time.time()
                print('Accuracy evaluation time: ' + str(t6_comb-t5_comb))
            
            if split_idx is not None: print "Split %d -"%(split_idx),
            if pca_comp: print 'PCA: %d -'%(pca_comp),
            if qstep:
                print 'Q step: %.3f - Levels: %d - Train SNR: %.2f - Test SNR: %.2f - '%\
                    (qstep,levels[param_comb_idx],snr[param_comb_idx,0],snr[param_comb_idx,1]),
            if split_idx is not None: print 'Rate: %.0f -'%(rate[param_comb_idx]),
            if split_idx is not None: print('Accuracy: %.4f'%(accs[param_comb_idx]))
            
            param_comb_idx += 1
            
    return accs,rate,snr,levels

def generate_print_report(results,filenameTxt):
    accs = results['accuracy']
    pca_comb_list = results['pca']
    levels_mean = results['level']
    qstep_comb_list = results['qstep']
    snr_mean = results['snr']
    rate_mean = results['rate']
    
    outFile = open(filenameTxt, "w")
    outFile.write("PCA\tQstep\tLevels\tRate\tSNR train\tSNR test\tAccuracy\n")
    print("PCA\tQstep\tLevels\tRate\tSNR train\tSNR test\tAccuracy")
    for par_idx in range(accs.size):
        acc = accs[par_idx]
        pca = pca_comb_list[par_idx]
        level = levels_mean[par_idx]
        qstep = qstep_comb_list[par_idx]
        dist_par = snr_mean[par_idx,:]
        if pca > 0:
            outFile.write("%d\t" % (pca))
            print "%d\t" % (pca),
        else:
            outFile.write("---\t")
            print "---\t",
        if level > 0 and qstep > 0:
            outFile.write("%.3f\t%d\t" % (qstep,level))
            print "%.3f\t%d\t" % (qstep,level),
        else:
            outFile.write("---\t---\t")
            print "---\t---\t",
            
        outFile.write("%d\t"%(rate_mean[par_idx]))
        print "%d\t"%(rate_mean[par_idx]),
        
        if level > 0 and qstep > 0:
            outFile.write("%.2f\t\t%.2f\t\t" % tuple(dist_par))
            print "%.2f\t\t%.2f\t\t" % tuple(dist_par),
        else:
            outFile.write("---\t\t---\t\t")
            print "---\t\t---\t\t",
        
        outFile.write("%.4f\n" % (acc.mean()))
        print("%.4f" % (acc.mean()))
    
    outFile.close()

def main():
    
    fct.createDir(ACC_PATH)
    fct.createDir(PCA_PATH)
    
    """
    Arguments parser
    """
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("features",help="path of the extracted features file")
    parser.add_argument("-p","--pca",help="pca with the provided number of components",type=int,nargs="+")
    parser.add_argument("-q","--quantization-step",help="quantization steps",type=float,nargs="+")
    parser.add_argument("--suffix",help="results suffix",nargs="?")
    args = parser.parse_args()
    
    # File prefix and path
    path_prefix = args.features
    if path_prefix.endswith('.'):
        path_prefix = path_prefix[:-1]
    filename_prefix = os.path.basename(path_prefix)
    platform,dataset,modelName = filename_prefix.split('_')
    
    print('Platform: ' + platform)
    print('Dataset: ' + dataset)
    print('Model: ' + modelName)
    if args.suffix:
        print('Suffix: ' + args.suffix)
    
    if args.pca:
        pca_comp_values = np.unique(args.pca).astype(np.int16)
    else:
        pca_comp_values = np.array([0, ],dtype=np.int16)
    
    if args.quantization_step:
        qstep_values = np.unique(args.quantization_step).astype(np.float32)
    else:
        qstep_values = np.array([0, ],dtype=np.float32)
        
    pca_comb_list = []
    qstep_comb_list = []
    for pca in pca_comp_values:
        for level in qstep_values:
            pca_comb_list += [pca]
            qstep_comb_list += [level]
    pca_comb_list = np.array(pca_comb_list,dtype=np.int16)
    qstep_comb_list = np.array(qstep_comb_list,dtype=np.float32)
    
    params_comb_num = pca_comp_values.size * qstep_values.size

    """
    Features and data loading
    """
    data_obj = fct.load(path_prefix)
    # Prepare data
    train_labels_list, test_labels_list, train_idxs_list, test_idxs_list = \
        fct.splitIdxLabel(data_obj)
    split_num = len(train_labels_list)
    
    print('Features loading ...')
    features = np.load(path_prefix + '.npy')
    
    # check features
    assert features.dtype == np.float32
        
    # reshape features
    features.shape = features.shape[0], -1
    
    # Cross validation
    print('Cross validation ...')
    
    accs = np.empty((params_comb_num,split_num),dtype=np.float32)
    rate = np.empty((params_comb_num,split_num),dtype=np.float32)
    snr = np.empty((params_comb_num,2,split_num),dtype=np.float32)
    levels = np.empty((params_comb_num,split_num),dtype=np.float32)
    
    t0 = time.time()
    for split_idx in range(split_num):
        result = split_accuracy(
                    features[train_idxs_list[split_idx]],
                    features[test_idxs_list[split_idx]],
                    train_labels_list[split_idx],
                    test_labels_list[split_idx],
                    pca_comp_values,
                    qstep_values,
                    split_idx, filename_prefix
                    )
        
        accs[:,split_idx] = result[0]
        rate[:,split_idx] = result[1]
        snr[:,:,split_idx] = result[2]
        levels[:,split_idx] = result[3]
        
        # ETA    
        t1 = time.time()
        print('Split ' + str(split_idx) + ' end' +\
        ' - Global start at: '+ time.strftime('%H:%M', time.gmtime(t0 - time.altzone)) + \
        ' - ETA: '+ time.strftime('%H:%M', time.gmtime((t1 - t0) / (split_idx + 1) * (split_num - split_idx - 1) + time.time() - time.altzone)))
    
    accs_mean = accs.mean(axis=-1)
    rate_mean = rate.mean(axis=-1)
    snr_mean = snr.mean(axis=-1)
    levels_mean = levels.mean(axis=-1)

    print("Cross validation execution time: " + str(time.time()-t0) +" s")

    filename_prefix += '_pca-'+'-'.join(map(str,pca_comp_values))
    filename_prefix += '_qstep-'+'-'.join(map(str,qstep_values))
    if args.suffix:
        filename_prefix += '_' + args.suffix
    
    """
    Accuracy saving
    """
    accuracy_path_prefix = os.path.join(ACC_PATH,filename_prefix) 
    print("Saving accuracy in " + accuracy_path_prefix + '.pickle')
    results = {'accuracy':accs_mean,
              'pca':pca_comb_list,
              'level':levels_mean,
              'rate':rate_mean,
              'qstep':qstep_comb_list,
              'snr':snr_mean}
    fct.save(results, accuracy_path_prefix);
    
    filenameTxt = accuracy_path_prefix + '.txt'
    print("Saving accuracy report in " + filenameTxt)
    generate_print_report(results,filenameTxt)
    
if __name__ == "__main__":
    main();