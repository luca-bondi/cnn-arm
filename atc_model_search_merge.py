#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
import argparse
import pickle
import os
import functions
import copy

MODEL_SEARCH_PATH = 'results/atc/search/'

def searchModel(models, model):
    idx = 0
    while idx <= len(models):
        if idx == len(models):
            return -1
        if models[idx] == model:
            return idx
        idx += 1 

def main():
    
    models = []
    timings = []
    accuracies = []
    
    # Arguments parser ---
    parser = argparse.ArgumentParser()
    parser.add_argument("file",help="pickle files containing model search results",nargs="+")
    args = parser.parse_args()
    
    # determine platforms and base model name
    platforms = []
    for filename in args.file:
        platform = os.path.basename(filename).split('_')[0] 
        if platform not in platforms:
            platforms += [platform]
    platforms_prefix = '-'.join(map(str,sorted(platforms)))
    print('Platforms: ' + platforms_prefix)
    
    base_model_name = os.path.basename(filename).split('_')[2]
    
    # merge files cycle ---
    for filename in args.file:
        #print filename
        par_l, par_c, tims, accs, base_model = pickle.load(open(filename.encode("utf-8"), "rb"))
        for parIdx, par in enumerate(par_c):
            model = functions.modelModify(base_model, par_l, par)
            modelIdx = searchModel(models, model)
            if modelIdx == -1:
                models += [copy.deepcopy(model)]
                modelIdx = len(models) - 1
                if not tims is None:
                    timings += [tims[parIdx]]
                else:
                    timings += [None]
                if not accs is None:
                    accuracies += [accs[parIdx]]
                else:
                    accuracies += [None]
            else:
                if timings[modelIdx] is None:
                    if not tims is None:
                        timings[modelIdx] = tims[parIdx]
                    else:
                        timings[modelIdx] = None
                if accuracies[modelIdx] is None:
                    if not accs is None:
                        accuracies[modelIdx] = accs[parIdx]
                    else:
                        accuracies[modelIdx] = None
    # Incomplete models pruning ---
    modelIdx = 0
    while modelIdx < len(models):
        if timings[modelIdx] is None or accuracies[modelIdx] is None:
            del models[modelIdx]
            del timings[modelIdx]
            del accuracies[modelIdx]
        else:
            modelIdx += 1
    
    assert len(models) == len(timings)
    assert len(models) == len(accuracies)
    
    # Save merged results ---
    merge_filename = os.path.join(MODEL_SEARCH_PATH,platforms_prefix+'_'+base_model_name.split('-s')[0] + '-wide.pickle')
    print('Output: ' + merge_filename)
    print('Models: ' + str(len(models)))
    pickle.dump((models, timings, accuracies), open(merge_filename, "wb"))
                
if __name__ == "__main__":
    main();
