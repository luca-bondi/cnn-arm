#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
Generate a .tex table for the model
"""
import argparse
import functions as fct
import cnnmodels
import os

MODEL_OUT_DIR = 'cnnmodels/'

def main():

    """
    Arguments parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("model",help="one of the following CNN models: " \
                        + ' '.join(sorted(cnnmodels.models.keys())))
    args = parser.parse_args()
    
    modelName  = args.model
    
    if not fct.checkModel(modelName):
        exit()
    
    model = cnnmodels.models[modelName]
    modelDescription = model['description']
    
    outString = "\\begin{tabular}{|l|l|c|} \n"
    """
    Model .tex table creation
    """
    for layerIdx, layer in enumerate(modelDescription):
        outString += "\\hline\n"
        outString += "\\multicolumn{3}{|c|}{\\textbf{Layer %d}} \\\\ \n" % (layerIdx)
        outString += "\\hline\n"
        for oper in layer:
            operName = oper[0]
            operParms = oper[1]
            
            if operName == 'fbcorr':
                outString += "\\multirow{2}{*}{fbcorr} & shape & %d x %d \\\\ \n" % operParms['initialize']['filter_shape']
                outString += "& filt num & %d \\\\ \n" % operParms['initialize']['n_filters']
                
                outString += "\\hline\n"
                outString += "\\multirow{2}{*}{threshold} "
                if operParms['kwargs']['min_out'] is None:
                    outString += "& th min & $\\infty$ \\\\ \n"
                else:
                    outString += "& th min & %d \\\\ \n" % operParms['kwargs']['min_out']
                    
                if operParms['kwargs']['max_out'] is None:
                    outString += "& th max & $\\infty$ \\\\ \n"
                else:
                    outString += "& th max & %d \\\\ \n" % operParms['kwargs']['max_out']
                
            elif operName == 'lpool':
                outString += "\\multirow{3}{*}{lpool} & shape & %d x %d \\\\ \n" % tuple(operParms['kwargs']['ker_shape'])
                outString += "& order & %d \\\\ \n" % operParms['kwargs']['order']
                outString += "& stride & %d \\\\ \n" % operParms['kwargs']['stride']
            
            elif operName == 'lnorm':
                outString += "\\multirow{3}{*}{lnorm} & shape & %d x %d \\\\ \n" % tuple(operParms['kwargs']['inker_shape'])
                outString += "& stretch & %d \\\\ \n" % operParms['kwargs']['stretch']
                outString += "& threshold & %d \\\\ \n" % operParms['kwargs']['threshold']
            
            outString += "\\hline\n"    
            
    outString += "\\end{tabular}"
    
    print(outString)
    
    outFile = open(os.path.join(MODEL_OUT_DIR,modelName+'.tex'),'w')
    outFile.write(outString)
    outFile.close()
    
if __name__ == "__main__":
    main();