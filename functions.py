# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
Create a directory if doesn't exist
"""
def createDir(path):
    import os
    if path is None or len(path) == 0: return
    if not os.path.isdir(path):
        os.makedirs(path)

"""
Check if model exists, otherwise prints the list of available models
"""
def checkModel(model):
    import cnnmodels
    if model not in cnnmodels.models:
        print('Model not recognized. Available models are: ')
        for mName in sorted(cnnmodels.models.keys()):
            print(mName)
        return False
    return True

"""
Given a model returns a tuple containing the features shape generate by the
model for a single image
"""
def featureShape(model):
    import numpy as np
    
    modelDescription = model['description']
    modelParams = model['input-params']
    
    imageSize = modelParams['imageSize']
    featuresShape = np.array(imageSize + (1,))
    for _, layerDesc in enumerate(modelDescription):
        for _, operDesc in enumerate(layerDesc):
            operName = operDesc[0]
            kwargs = operDesc[1]['kwargs']
            if operName == 'lnorm':
                featuresShape[0:2] -= np.array(kwargs['outker_shape']) - 1
            elif operName == 'fbcorr':
                init = operDesc[1]['initialize']
                featuresShape[0:2] -= np.array(init['filter_shape']) - 1
                featuresShape[2] = init['n_filters']
            elif operName == 'lpool':
                featuresShape[0:2] -= np.array(kwargs['ker_shape']) - 1
                featuresShape[0:2] = np.ceil(featuresShape[0:2].astype(np.float32) / kwargs['stride'])
                
    return tuple(featuresShape.tolist())
"""
Given a model returns the list of layers and operations together with
their output features shape 
"""
def modelFeatureShape(model):
    import numpy as np
    modelParameters = model['input-params']
    modelDescription = model['description']
    features = {}
    imageSize = modelParameters['imageSize']
    features['input-shape'] = imageSize;
    features['network'] = []
    featuresShape = np.array(imageSize + (1,))
    for _, layerDesc in enumerate(modelDescription):
        opsList = [];
        for _, operDesc in enumerate(layerDesc):
            opDict = {}
            operName = operDesc[0]
            opDict['name'] = operName;
            kwargs = operDesc[1]['kwargs']
            if operName == 'lnorm':
                featuresShape[0:2] -= np.array(kwargs['outker_shape']) - 1
                opDict['params'] = (kwargs['outker_shape'],)
            elif operName == 'fbcorr':
                init = operDesc[1]['initialize']
                featuresShape[0:2] -= np.array(init['filter_shape']) - 1
                featuresShape[2] = init['n_filters']
                opDict['params'] = (init['filter_shape'],init['n_filters'])
            elif operName == 'lpool':
                featuresShape[0:2] -= np.array(kwargs['ker_shape']) - 1
                featuresShape[0:2] = np.ceil(featuresShape[0:2].astype(np.float32) / kwargs['stride'])
                opDict['params'] = (kwargs['ker_shape'],kwargs['stride'])
            opDict['features'] = tuple(featuresShape.tolist())
            opsList += [opDict];
        features['network'] += [opsList];
    return features
"""
Modifies a given model applying the given parameters
"""
def modelModify(cnnModel, parameters_labels, parameters):
    from copy import deepcopy
    cnnDescr = deepcopy(cnnModel['description'])
    cnnParameters = deepcopy(cnnModel['input-params'])
    for parIdx, parName in enumerate(parameters_labels):
        parVal = parameters[parIdx]
        if parName == 'L0_norm_shape':
            cnnDescr[0][0][1]['kwargs']['inker_shape'] = [parVal, parVal]
            cnnDescr[0][0][1]['kwargs']['outker_shape'] = [parVal, parVal]
        elif parName == 'L0_norm_stretch':
            cnnDescr[0][0][1]['kwargs']['stretch'] = parVal
        elif parName == 'L0_norm_th':
            cnnDescr[0][0][1]['kwargs']['threshold'] = parVal
            
        elif parName == 'L1_filt_shape':
            cnnDescr[1][0][1]['initialize']['filter_shape'] = (parVal, parVal)
        elif parName == 'L1_filt_num':
            cnnDescr[1][0][1]['initialize']['n_filters'] = parVal
        elif parName == 'L1_act_min':
            cnnDescr[1][0][1]['kwargs']['min_out'] = parVal
        elif parName == 'L1_act_max':
            cnnDescr[1][0][1]['kwargs']['max_out'] = parVal
        elif parName == 'L1_pool_shape':
            cnnDescr[1][1][1]['kwargs']['ker_shape'] = [parVal, parVal]
        elif parName == 'L1_pool_exp':
            cnnDescr[1][1][1]['kwargs']['order'] = parVal
        elif parName == 'L1_norm_shape':
            cnnDescr[1][2][1]['kwargs']['inker_shape'] = [parVal, parVal]
            cnnDescr[1][2][1]['kwargs']['outker_shape'] = [parVal, parVal]
        elif parName == 'L1_norm_stretch':
            cnnDescr[1][2][1]['kwargs']['stretch'] = parVal
        elif parName == 'L1_norm_th':
            cnnDescr[1][2][1]['kwargs']['threshold'] = parVal
            
        elif parName == 'L2_filt_shape':
            cnnDescr[2][0][1]['initialize']['filter_shape'] = (parVal, parVal)
        elif parName == 'L2_filt_num':
            cnnDescr[2][0][1]['initialize']['n_filters'] = parVal
        elif parName == 'L2_act_min':
            cnnDescr[2][0][1]['kwargs']['min_out'] = parVal
        elif parName == 'L2_act_max':
            cnnDescr[2][0][1]['kwargs']['max_out'] = parVal
        elif parName == 'L2_pool_shape':
            cnnDescr[2][1][1]['kwargs']['ker_shape'] = [parVal, parVal]
        elif parName == 'L2_pool_exp':
            cnnDescr[2][1][1]['kwargs']['order'] = parVal
        elif parName == 'L2_norm_shape':
            cnnDescr[2][2][1]['kwargs']['inker_shape'] = [parVal, parVal]
            cnnDescr[2][2][1]['kwargs']['outker_shape'] = [parVal, parVal]
        elif parName == 'L2_norm_stretch':
            cnnDescr[2][2][1]['kwargs']['stretch'] = parVal
        elif parName == 'L2_norm_th':
            cnnDescr[2][2][1]['kwargs']['threshold'] = parVal
            
    newCnnModel = {'input-params': cnnParameters, 'description': cnnDescr}
    return newCnnModel

"""
Labels and indexes for each split for both train and test
"""
def splitIdxLabel(dataObj):
    train_labels_list = [];
    test_labels_list = [];
    train_idxs_list = [];
    test_idxs_list = [];

    for split in range(dataObj.n_splits): 
        _, train_labels, train_idxs = \
            dataObj.classification_task(split=split, split_role='train')
    
        _, test_labels, test_idxs = \
            dataObj.classification_task(split=split, split_role='test')
        
        train_labels_list += [train_labels]
        test_labels_list += [test_labels]
        train_idxs_list += [train_idxs]
        test_idxs_list += [test_idxs]
    
    return train_labels_list, test_labels_list, train_idxs_list, test_idxs_list

"""
Merges a couple of model search results (time,accuracy)
"""
def modelSearchResultsMerge(result1, result2):
    if result1 is None and result2 is None:
        print("Empty results")
        return
    
    if result1 != None:
        par_l1, par_comb1, tim1, acc1, base_model1 = result1
    if result2 != None:
        par_l2, par_comb2, tim2, acc2, base_model2 = result2
    
    if result1 is None:
        if tim2 is None or acc2 is None:
            print("Incomplete results")
            return
        else:
            return result2
    
    if result2 is None:
        if tim1 is None or acc1 is None:
            print("Incomplete results")
            return 
        else:
            return result1
    
    if set(par_l1) != set(par_l2):
        print("Different parameters labels")
        return
    if set(par_comb1) != set(par_comb2):
        print("Different parameters values")
        return
    if set(base_model1) != set(base_model2):
        print("Different base models")
        return
    if tim1 is None and tim2 is None:
        print("Timings missing")
        return
    if acc1 is None and acc2 is None:
        print("Accuracy missing")
        return
    if tim1 != None and tim2 != None:
        print("Double timings")
        return
    if acc1 != None and acc2 != None:
        print("Double accuracy")
        return
    
    results = (par_l1, par_comb1)
    
    if tim1 != None:
        results += (tim1,)
    else:
        results += (tim2,)
    
    if acc1 != None:
        results += (acc1,)
    else:
        results += (acc2,)
    
    results += (base_model1,)
    
    return results

"""
Given a list of models extracts a base model and the set of labels and parameters
"""
def modelsCommonDiff(models):
    
    assert len(models) > 0
    
    if len(models) == 1:
        return models[0], None, None
    
    par_labels_whole = [
        'L0_norm_shape' ,
        'L0_norm_stretch' ,
        'L0_norm_th' ,

        'L1_filt_shape' ,
        'L1_filt_num' ,
        'L1_act_min',
        'L1_act_max',
        'L1_pool_shape' ,
        'L1_pool_exp' ,
        'L1_norm_shape' ,
        'L1_norm_stretch' ,
        'L1_norm_th' ,

        'L2_filt_shape' ,
        'L2_filt_num' ,
        'L2_act_min',
        'L2_act_max',
        'L2_pool_shape' ,
        'L2_pool_exp' ,
        'L2_norm_shape' ,
        'L2_norm_stretch' ,
        'L2_norm_th'
        ]
    
    base_model = models[0]
    par_labels = []
    par_combs = []
    
    for parName in par_labels_whole:
        if parName == 'L0_norm_shape':
            parValRef = models[0]['description'][0][0][1]['kwargs']['inker_shape'][0]
            same = True
            for model in models:
                if (model['description'][0][0][1]['kwargs']['inker_shape'][0] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []    
                for model in models:
                    parVals += [model['description'][0][0][1]['kwargs']['inker_shape'][0]]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L0_norm_stretch':
            parValRef = models[0]['description'][0][0][1]['kwargs']['stretch']
            same = True
            for model in models:
                if (model['description'][0][0][1]['kwargs']['stretch'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][0][0][1]['kwargs']['stretch']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L0_norm_th':
            parValRef = models[0]['description'][0][0][1]['kwargs']['threshold']
            same = True
            for model in models:
                if (model['description'][0][0][1]['kwargs']['threshold'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][0][0][1]['kwargs']['threshold']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
                # Layer 1 ----------------------------------
            
        elif parName == 'L1_filt_shape':
            parValRef = models[0]['description'][1][0][1]['initialize']['filter_shape'][0]
            same = True
            for model in models:
                if (model['description'][1][0][1]['initialize']['filter_shape'][0] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][1][0][1]['initialize']['filter_shape'][0]]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_filt_num':
            parValRef = models[0]['description'][1][0][1]['initialize']['n_filters']
            same = True
            for model in models:
                if (model['description'][1][0][1]['initialize']['n_filters'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][1][0][1]['initialize']['n_filters']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_act_min':
            parValRef = models[0]['description'][1][0][1]['kwargs']['min_out']
            same = True
            for model in models:
                if (model['description'][1][0][1]['kwargs']['min_out'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][1][0][1]['kwargs']['min_out']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_act_max':
            parValRef = models[0]['description'][1][0][1]['kwargs']['max_out']
            same = True
            for model in models:
                if (model['description'][1][0][1]['kwargs']['max_out'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][1][0][1]['kwargs']['max_out']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_pool_shape':
            parValRef = models[0]['description'][1][1][1]['kwargs']['ker_shape'][0]
            same = True
            for model in models:
                if (model['description'][1][1][1]['kwargs']['ker_shape'][0] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][1][1][1]['kwargs']['ker_shape'][0]]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_pool_exp':
            parValRef = models[0]['description'][1][1][1]['kwargs']['order']
            same = True
            for model in models:
                if (model['description'][1][1][1]['kwargs']['order'] != parValRef):
                    same = False
                    break;
            parVals = []
            for model in models:
                parVals += [model['description'][1][1][1]['kwargs']['order']]
            if not same:
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_norm_shape':
            parValRef = models[0]['description'][1][2][1]['kwargs']['inker_shape'][0]
            same = True
            for model in models:
                if (model['description'][1][2][1]['kwargs']['inker_shape'][0] != parValRef):
                    same = False
                    break;
            parVals = []
            for model in models:
                parVals += [model['description'][1][2][1]['kwargs']['inker_shape'][0]]
            if not same:
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_norm_stretch':
            parValRef = models[0]['description'][1][2][1]['kwargs']['stretch']
            same = True
            for model in models:
                if (model['description'][1][2][1]['kwargs']['stretch'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][1][2][1]['kwargs']['stretch']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L1_norm_th':
            parValRef = models[0]['description'][1][2][1]['kwargs']['threshold']
            same = True
            for model in models:
                if (model['description'][1][2][1]['kwargs']['threshold'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][1][2][1]['kwargs']['threshold']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
        
                # Layer 2 ---------------------------------- 
                
        elif parName == 'L2_filt_shape':
            parValRef = models[0]['description'][2][0][1]['initialize']['filter_shape'][0]
            same = True
            for model in models:
                if (model['description'][2][0][1]['initialize']['filter_shape'][0] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][0][1]['initialize']['filter_shape'][0]]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_filt_num':
            parValRef = models[0]['description'][2][0][1]['initialize']['n_filters']
            same = True
            for model in models:
                if (model['description'][2][0][1]['initialize']['n_filters'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][0][1]['initialize']['n_filters']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_act_min':
            parValRef = models[0]['description'][2][0][1]['kwargs']['min_out']
            same = True
            for model in models:
                if (model['description'][2][0][1]['kwargs']['min_out'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][0][1]['kwargs']['min_out']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_act_max':
            parValRef = models[0]['description'][2][0][1]['kwargs']['max_out']
            same = True
            for model in models:
                if (model['description'][2][0][1]['kwargs']['max_out'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][0][1]['kwargs']['max_out']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_pool_shape':
            parValRef = models[0]['description'][2][1][1]['kwargs']['ker_shape'][0]
            same = True
            for model in models:
                if (model['description'][2][1][1]['kwargs']['ker_shape'][0] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][1][1]['kwargs']['ker_shape'][0]]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_pool_exp':
            parValRef = models[0]['description'][2][1][1]['kwargs']['order']
            same = True
            for model in models:
                if (model['description'][2][1][1]['kwargs']['order'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][1][1]['kwargs']['order']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_norm_shape':
            parValRef = models[0]['description'][2][2][1]['kwargs']['inker_shape'][0]
            same = True
            for model in models:
                if (model['description'][2][2][1]['kwargs']['inker_shape'][0] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][2][1]['kwargs']['inker_shape'][0]]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_norm_stretch':
            parValRef = models[0]['description'][2][2][1]['kwargs']['stretch']
            same = True
            for model in models:
                if (model['description'][2][2][1]['kwargs']['stretch'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][2][1]['kwargs']['stretch']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
                
        elif parName == 'L2_norm_th':
            parValRef = models[0]['description'][2][2][1]['kwargs']['threshold']
            same = True
            for model in models:
                if (model['description'][2][2][1]['kwargs']['threshold'] != parValRef):
                    same = False
                    break;
            if not same:
                parVals = []
                for model in models:
                    parVals += [model['description'][2][2][1]['kwargs']['threshold']]
                par_labels += [parName]
                par_combs += [list(set(parVals))]
        
    return base_model, par_labels, par_combs
    
"""
Save and load generic data wrappers using pickle
"""
def save(obj, filePath):
    import pickle
    import os
    if not filePath.endswith('.pickle'):
        filePath += '.pickle'
    createDir(os.path.dirname(filePath))
    pickle.dump(obj, open(filePath, 'wb'))

"""
Read the specified pickle file. ".pickle" extension can be missed
@throws IOExept if file not found
"""
def load(filePath):
    import pickle
    if filePath.endswith('.'): filePath = filePath[:-1]
    if not filePath.endswith('.pickle'): filePath += '.pickle'
    obj = pickle.load(open(filePath, 'rb'))
    return obj

def featuresNormalize(trainSet, testSet):
    
    # Calculate normalizing parameters
    fmean = trainSet.mean(axis=0)
    fstd = trainSet.std(axis=0)
    fstd[fstd == 0.] = 1.
    
    # Apply train and test set normalization
    trainSet -= fmean
    trainSet /= fstd
    testSet -= fmean
    testSet /= fstd
    
    return trainSet,testSet
    
def featuresUnbias(trainSet, testSet):
    
    fmean = trainSet.mean(axis=0)
    
    trainSet -= fmean
    testSet -= fmean
    
    return trainSet,testSet

# Timing statistics on total time from cnn timing format ---
def timing_stats(timing_list):
    
    img_num = len(timing_list)
    
    time_mean = 0
    for timing_spec in timing_list:
        time_mean += timing_spec[0]
    time_mean /= img_num
    
    time_std = 0
    for timing_spec in timing_list:
        time_std += (timing_spec[0]-time_mean)**2
    time_std /= img_num-1
    time_std = time_std**0.5
    
    return time_mean, time_std
    