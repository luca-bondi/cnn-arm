#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
- Features and data loading
- SVM train and test with 10 fold cross validation
"""
import argparse
import os
import time
import numpy as np

import functions as fct
from svm_mt import svm_ova

ACC_PATH = 'results/cta/accuracy/'


def split_accuracy(train_set,test_set,train_labels,test_labels,split_idx=None,split_num=None,t0=None):
    """
    Accuracy of a split
    @param train_set
    @param test_set
    @param train_labels
    @param test_labels
    @param split_idx for ETA estimation
    @param split_num for ETA estimation
    @param t0 for ETA estimation
    @return accuracy of the split np.float32
    """
    
    train_set,test_set = fct.featuresNormalize(train_set,test_set)
    acc = svm_ova(train_set,test_set,train_labels,test_labels)
            
    if split_idx is not None:
        t1 = time.time()
        print('Split ' + str(split_idx) + ' accuracy: %07.4f'%(acc) + ' - ETA: '+ \
            time.strftime('%H:%M', time.gmtime((t1 - t0) / (split_idx + 1) * (split_num - split_idx - 1) + time.time() - time.altzone)))
    
    return acc

def main():
    """
    Arguments parser
    """
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("-d","--data",help="path to the data file",required=True)
    parser.add_argument("-f","--features",help="prefix path to the extracted features files",required=True)
    parser.add_argument("-s","--size",help="path to the size file",required=True)
    args = parser.parse_args()
    
    # File prefix and path
    filename_prefix = os.path.basename(args.features)
    if filename_prefix.endswith('.'):
        filename_prefix = filename_prefix[:-1]
    platform,dataset,modelName = filename_prefix.split('_')
    print('Platform: ' + platform)
    print('Dataset: ' + dataset)
    print('Model: ' + modelName)
    
    """
    Data loading
    """
    data_obj = fct.load(args.data)
    # Prepare data
    train_labels_list, test_labels_list, train_idxs_list, test_idxs_list = \
        fct.splitIdxLabel(data_obj)
        
    split_num = len(train_labels_list)
    """
    Size loading
    """
    size_dict = fct.load(args.size)
    
    """
    Cycle on QF and determine accuracy
    """
    qf_list = size_dict['quality']
    accuracies = np.empty(len(qf_list))
    
    t0 = time.time()
    for qf_idx,qf in enumerate(qf_list):
        
        print('JPEG QF: ' + str(qf))
        
        # features loading
        print('Features loading ...')
        features = np.load(args.features + '_qf-' + str(qf) + '.npy')
        
        # check features
        assert features.dtype == np.float32
        
        # reshape features
        features.shape = features.shape[0], -1
        
        # Cross validation
        print('Cross validation ...')
        t0_qf = time.time()
        accs = np.empty(split_num)
        for split_idx in range(split_num):
            accs[split_idx] = split_accuracy(features[train_idxs_list[split_idx]],
                                  features[test_idxs_list[split_idx]],
                                  train_labels_list[split_idx],
                                  test_labels_list[split_idx],
                                  split_idx, split_num, t0_qf)

        accuracies[qf_idx] = accs.mean()
        t1 = time.time()
        print('QF: ' + str(qf) + ' - mean accuracy: ' + str(accuracies[qf_idx]) + ' - Global ETA : '+ \
            time.strftime('%H:%M', time.gmtime((t1 - t0) / (qf_idx + 1) * (len(qf_list) - qf_idx - 1) + time.time() - time.altzone)))
    
    print('\n--------')
    print('Total accuracy evaluation time: ' + str(time.time() - t0) + 's')
    print('--------') 
    
    accuracy_path_prefix = os.path.join(ACC_PATH,filename_prefix)
    print('Saving accuracy in ' + accuracy_path_prefix + '.pickle')
    fct.save({'quality':qf_list,'accuracy':accuracies,'size':size_dict['size']},accuracy_path_prefix);
    
    filenameTxt = accuracy_path_prefix + '.txt'
    print("Saving accuracy report in " + filenameTxt)
    outFile = open(filenameTxt, "w")
    
    outFile.write("QF\tAcc[%]\n")
    print('QF\tAcc[%]')
    for qf_idx,qf in enumerate(qf_list):
        outFile.write('%02d\t%.4f\n'%(qf,accuracies[qf_idx]))
        print('%02d\t%.4f'%(qf,accuracies[qf_idx]))
    
if __name__ == "__main__":
    main();