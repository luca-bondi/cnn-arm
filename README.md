#Rate-Energy-Accuracy Optimization of Convolutional Neural Networks for Face Recognition#

## Authors ##
* Luca Bondi <luca.bondi@polimi.it>
* Luca Baroffio <luca.baroffio@polimi.it>
* Matteo Cesana <matteo.cesana@polimi.it>
* Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
* Giovani Chiachia <chiachia@ic.unicamp.br>
* Anderson Rocha <rocha@ic.unicamp.br>

Based on https://github.com/giovanichiachia/pubfig83-baseline

### Requirements ###
* PubFig83 aligned dataset from http://ic.unicamp.br/~chiachia/resources/pubfig83-aligned (in ../pf83aligned-whole/)
* CNN implementation from https://github.com/luca-bondi/convnet-rfw (cnnrandom folder in ./)
* Run *requirements.py* to check for necessary Python modules and versions

## Paper plots and tables ##
```sh
./paper_plots.sh
./paper_tables.sh
```

## Performance dataset generation ##
```sh
./dataset-performance-generate.py
```

## ATC features extraction using CNN ##
### Time profiling (on low power device) ###
```sh
./atc_features.py datasetPath model platformPrefix -t 
```
Example:
```sh
./atc_features.py ../pf83aligned-performance/ fg11-ht-l3-1 rpi -t
./atc_features.py ../pf83aligned-performance/ fg11-ht-l2-opt rpi -t
```
### Features extraction ###
```sh
./atc_features.py datasetPath model platformPrefix -f [-m number_of_processes] [-b batch_size]
```
Example:
```sh
./atc_features.py ../pf83aligned-whole/ fg11-ht-l3-1 pc -f -m 4 
./atc_features.py ../pf83aligned-whole/ fg11-ht-l2-opt pc -f -m 4 
```
## ATC accuracy evaluation ##
```sh
./atc_accuracy.py features [-p pca_num_comp [pca_num_comp ...]] [-l quantization_step [quantization_step ...]]
```
Example:
```sh
./atc_accuracy.py results/atc/features/pc_pf83aligned-whole_fg11-ht-l2-opt. -p 1600 3200 6400 -q 0 0.005 0.01 0.02 0.03 0.05
```

## ATC CNN model search ##
### Models timing (on low power device) ###
```sh
./atc_model_search.py datasetPath model platformPrefix -t 
```
Example:
```sh
./atc_model_search.py ../pf83aligned-performance/ fg11-ht-l2-s1 rpi -t
./atc_model_search.py ../pf83aligned-performance/ fg11-ht-l2-s2 rpi -t
./atc_model_search.py ../pf83aligned-performance/ fg11-ht-l2-s3 rpi -t
./atc_model_search.py ../pf83aligned-performance/ fg11-ht-l2-s4 rpi -t
./atc_model_search.py ../pf83aligned-performance/ fg11-ht-l2-s5 rpi -t
```
### Models accuracy ###
```sh
./atc_model_search.py datasetPath model platformPrefix -a [-m number_of_processes]
```
Example:
```sh
./atc_model_search.py ../pf83aligned-whole/ fg11-ht-l2-s1 pc -a -m 4
./atc_model_search.py ../pf83aligned-whole/ fg11-ht-l2-s2 pc -a -m 4
./atc_model_search.py ../pf83aligned-whole/ fg11-ht-l2-s3 pc -a -m 4
./atc_model_search.py ../pf83aligned-whole/ fg11-ht-l2-s4 pc -a -m 4
./atc_model_search.py ../pf83aligned-whole/ fg11-ht-l2-s5 pc -a -m 4
```
### Model search merge ###
```sh
./atc_model_search_merge.py file [file ...]
```
Example:
```sh
./atc_model_search_merge.py \
	results/atc/search/pc_pf83aligned-performance_fg11-ht-l2-s1.pickle \
	results/atc/search/pc_pf83aligned-performance_fg11-ht-l2-s2.pickle \
	results/atc/search/pc_pf83aligned-performance_fg11-ht-l2-s3.pickle \
	results/atc/search/pc_pf83aligned-performance_fg11-ht-l2-s4.pickle \
	results/atc/search/pc_pf83aligned-performance_fg11-ht-l2-s5.pickle \
	results/atc/search/rpi_pf83aligned-performance_fg11-ht-l2-s1.pickle \
	results/atc/search/rpi_pf83aligned-performance_fg11-ht-l2-s2.pickle \
	results/atc/search/rpi_pf83aligned-performance_fg11-ht-l2-s3.pickle \
	results/atc/search/rpi_pf83aligned-performance_fg11-ht-l2-s4.pickle \
	results/atc/search/rpi_pf83aligned-performance_fg11-ht-l2-s5.pickle
```

### Model search plot ###
```sh
./atc_model_search_plot.py [-r reference_accuracy] file [file ...]
```
Example:
```sh
./atc_model_search_plot.py \
	results/atc/search/pc_pf83aligned-whole_fg11-ht-l2-s1.pickle \
	results/atc/search/rpi_pf83aligned-performance_fg11-ht-l2-s1.pickle \
	-r 87.13
./atc_model_search_plot.py results/atc/search/results/atc/search/pc-rpi_fg11-ht-l2-wide.pickle
```

## CTA features extraction using CNN ##
### Time profiling (on low power device) ###
```sh
./cta_features.py datasetPath model platformPrefix -qf quality_factor [quality_factor ...] -t 
```
Example:
```sh
./cta_features.py ../pf83aligned-performance/ fg11-ht-l3-1 rpi -qf 10 30 50 70 90 -t
./cta_features.py ../pf83aligned-performance/ fg11-ht-l2-opt rpi -qf 10 30 50 70 90 -t
```
### Extraction ###
```sh
./cta_features.py datasetPath model platformPrefix -qf quality_factor [quality_factor ...] -f [-m number_of_processes]
```
Example:
```sh
./cta_features.py ../pf83aligned-whole/ fg11-ht-l3-1 pc -qf 10 30 50 70 90 -f -m 4 
./cta_features.py ../pf83aligned-whole/ fg11-ht-l2-opt pc -qf 10 30 50 70 90 -f -m 4 
```

## CTA accuracy evaluation ##
```sh
./cta_accuracy.py -d data -f features -s size
```
Example:
```sh
./cta_accuracy.py \
	-d results/cta/features/pc_pf83aligned-whole_fg11-ht-l3-1 .pickle \
	-s results/cta/size/pc_pf83aligned-whole_fg11-ht-l3-1 .pickle \
	-f results/cta/features/pc_pf83aligned-whole_fg11-ht-l3-1 
./cta_accuracy.py \
	-d results/cta/features/pc_pf83aligned-whole_fg11-ht-l2-opt.pickle \
	-s results/cta/size/pc_pf83aligned-whole_fg11-ht-l2-opt.pickle \
	-f results/cta/features/pc_pf83aligned-whole_fg11-ht-l2-opt
```

## ATC vs. CTA plot ##
```sh
./atc_cta_plot.py -a atc_accuracy -c cta_accuracy [cta_accuracy [cta_accuracy ...]] [--ref-l2 reference_l2] [--ref-l3 reference_l3]
```
Example:
```sh
./atc_cta_plot.py \
	-a results/atc/accuracy/pc_pf83aligned-whole_fg11-ht-l2-opt_pca-1600-3200-6400_qstep-0.0-0.005-0.01-0.03-0.05.pickle \
	-c results/cta/accuracy/pc_pf83aligned-whole_fg11-ht-l2-opt.pickle \
	results/cta/accuracy/pc_pf83aligned-whole_fg11-ht-l3-1.pickle \
	--ref-l2 86.73 --ref-l3 87.13
```