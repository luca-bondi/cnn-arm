#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
- Image loading
- Model generation
- CNN features extraction
- <Model accuracy evaluation|Timing saving>
"""

import itertools
import argparse
import pickle
import time
import numpy as np
from scipy import misc
from copy import deepcopy

import cnnmodels
import cnnrandom as cnn
import data
import functions as fct

MODEL_SEARCH_PATH = 'results/atc/search/'

BATCH_SIZE_DEFAULT = 100

def extract_features(featuresExtractor, imgs):
    return featuresExtractor.extract(imgs)

def main():

    # Arguments parser ---
    parser = argparse.ArgumentParser()
    parser.add_argument("datasetPath",help="path of the image dataset")
    parser.add_argument("model",help="one of the following CNN models: " \
                        + ' '.join(sorted(cnnmodels.models.keys())))
    parser.add_argument("platformPrefix",help="platform prefix used as output filename prefix")

    group = parser.add_mutually_exclusive_group()
    group.required = True
    group.add_argument("-t","--timing",help="perform time profiling",action="store_true")
    group.add_argument("-a","--accuracy",help="perform accuracy test",action="store_true")
    
    parser.add_argument("-m","--processes",help="number of processes used to perform feature extraction",type=int,nargs=1)
    parser.add_argument("-b","--batch-size",help="number of images in a single batch",type=int,nargs=1)
    
    args = parser.parse_args()

    datasetPath = args.datasetPath
    datasetName = datasetPath.rsplit('/')[-1]
    if len(datasetName) == 0:
        datasetName = datasetPath.rsplit('/')[-2]
        
    platformPrefix = args.platformPrefix
    
    accuracyFlag = False
    timingFlag = False
    if args.timing: timingFlag = True
    
    if args.accuracy:
        accuracyFlag = True
        from atc_accuracy import split_accuracy
        
    
    # MultiTheading check ---
    multi_processes = False
    if args.processes:
        multi_processes = args.processes[0]
        
    if multi_processes != False and multi_processes > 1:
        try:
            import multiprocessing
            if multiprocessing.cpu_count() > 1:
                from joblib import Parallel, delayed
            else:
                multi_processes = False
        except ImportError:
            multi_processes = False
    else:
        multi_processes = False
        
    # Batch size check ---
    batch_size = BATCH_SIZE_DEFAULT
    if args.batch_size:
        batch_size = args.batch_size[0]
    
    baseModelName = args.model
    
    baseFilename = platformPrefix + '_' + datasetName + '_' + baseModelName

    # CNN initialization ---
    if not fct.checkModel(baseModelName):
        exit();

    model = deepcopy(cnnmodels.models[baseModelName])
    modelDescr = model['description']
    modelParams = model['input-params']

    image_shape = modelParams['imageSize']
    
    # Parameters setup ---
    fct.createDir(MODEL_SEARCH_PATH)
    
    parameters_list = cnnmodels.model_search[baseModelName]
    
    parameters_labels = []
    parameters_values = []
    for param in parameters_list:
        parameters_labels += [param[0]]
        parameters_values += [param[1]]
    
    parameters_comb = list(itertools.product(*parameters_values))
    
    num_comb = len(parameters_comb)
    
    # Images list and SVM prepare ---
    
    dataObj = data.cvprw11_setup(datasetPath)
    imgsPath = dataObj.classification_task()[0]
    imgsNum = len(imgsPath)
    
    n_splits = dataObj.n_splits
    
    if accuracyFlag:
        train_labels_list, test_labels_list, train_idxs_list, test_idxs_list = \
        fct.splitIdxLabel(dataObj)
        accs_mean = np.empty(num_comb,dtype=np.float32)
    if timingFlag:
        timings_tot = np.empty(num_comb,dtype=np.float32)
    
    # Load all images ---
    print('Loading images ...')
    imgs = np.empty((imgsNum, ) + image_shape,dtype=np.float32)
    for img_idx, path in enumerate(imgsPath):
        # Image loading and resizing 
        img = misc.imread(path, flatten=True)
        img = misc.imresize(img, image_shape).astype(np.float32)
        
        # image scaling between 0 and 1
        img -= img.min()
        img /= img.max()
        
        imgs[img_idx,:,:] = img
        
        if (img_idx + 1) % batch_size == 0 :
            print('Loaded %5d of %5d images' %(img_idx + 1,imgsNum))
    
    # Parameters cycle ---
    t0 = time.time();
    for parCombIdx, parameters in enumerate(parameters_comb):
        
        print("Combination " + str(parCombIdx+1) + "/" + str(num_comb))
        print(parameters)
        
        # Model setup ---
        model = fct.modelModify(model,parameters_labels,parameters)
        modelDescr = model['description']
        featuresShape = fct.featureShape(model)
        featuresExtractor = cnn.BatchExtractor(image_shape, modelDescr)
    
        timings = imgsNum * [None]
        features = np.empty((imgsNum,) + featuresShape, dtype=np.float32)
        
        # Features extraction ---
        if not timingFlag and multi_processes != False:
            print("Images features extraction with " + str(multi_processes) + " processes")
            batch_num = np.ceil(imgsNum/float(batch_size)).astype(np.int16)
            results = Parallel(n_jobs=multi_processes)(delayed(extract_features)(featuresExtractor,imgs[batch_idx*batch_size:(batch_idx+1)*batch_size,:,:])
                                                   for batch_idx in range(batch_num))
            for batch_idx,result in enumerate(results):
                idxs = np.arange(batch_idx*batch_size,min((batch_idx+1)*batch_size,imgsNum))
                if accuracyFlag: features[idxs,:,:] = result[0]
                if timingFlag: timings[idxs] = result[1]/batch_size
        
        else:
            print("Images features extraction")
            for img_idx in range(imgsNum):
                img = imgs[img_idx,:,:]
                img.shape = (1,) + img.shape
                features[img_idx], timings[img_idx] = featuresExtractor.extract(img)
    
        # Show and store timings ---
        if timingFlag:
            time_mean, time_std = fct.timing_stats(timings)
            timings_tot[parCombIdx] = time_mean
            print('Average feature extraction time per image: {:3.4f}s (+/-{:2.2f}%)'.format(time_mean,time_std/time_mean))
        
        """
        SVM train and test
        """
        if accuracyFlag:
    
            features.shape = features.shape[0], -1
            
            print("Accuracy evaluation")
            # Single thread execution
            accs = np.empty(n_splits, dtype=np.float32)
            for split in range(n_splits):
                accs[split],_,_,_ = split_accuracy(\
                    features[train_idxs_list[split]],features[test_idxs_list[split]],\
                    train_labels_list[split],test_labels_list[split])
            
            print('Accuracy: %g (+/-%g) %%' % (
                accs.mean(), accs.std(ddof=1) / np.sqrt(n_splits)))
    
            accs_mean[parCombIdx] = accs.mean()
    
        # ETA    
        t1 = time.time()
        print('Combination ' + str(parCombIdx) + ' end' +\
        ' - Global start at: '+ time.strftime('%d-%m %H:%M', time.gmtime(t0 - time.altzone)) + \
        ' - ETA: '+ time.strftime('%d-%m %H:%M', time.gmtime((t1 - t0) / (parCombIdx + 1) * (num_comb - parCombIdx - 1) + time.time() - time.altzone)))

            
    results = (parameters_labels,parameters_comb)
    if timingFlag:
        results += (timings_tot,)
    else:
        results += (None, )
    if accuracyFlag:
        results += (accs_mean,)
    else:
        results += (None, )
    results += (cnnmodels.models[baseModelName],)
        
    filename = MODEL_SEARCH_PATH + baseFilename + '.pickle'
    pickle.dump(results, open(filename, "wb"))
    
    print("Results saved in " + filename)

if __name__ == "__main__":
    main();
