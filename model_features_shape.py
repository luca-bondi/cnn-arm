#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
Print model layers and operations together with the features shape
at the end of each operation
"""
import cnnmodels
import functions
import argparse

def main():
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("model",help="one of the following CNN models: " \
                        + ' '.join(sorted(cnnmodels.models.keys())))
    args = parser.parse_args()
    
    if not functions.checkModel(args.model):
        exit()
                                           
    model_shape = functions.modelFeatureShape(cnnmodels.models[args.model])
    input_shape = model_shape['input-shape']
    network = model_shape['network']
    
    #pprint(model_shape)
    
    print 'Input shape: ' + str(input_shape)
    
    print '+' + '-'*(4*13) + '+'
    print '| Operation\tNBsize\tNfilt/Stride\tOutput shape |'
    print '+' + '-'*(4*13) + '+'
    for layer_idx, layer in enumerate(network):
        print ' ---- Layer ' + str(layer_idx) + ' ----'
        for op_idx, op in enumerate(layer):
            print ' ' + op['name'] + '\t\t',
            print ' ' + 'x'.join(map(str,list(op['params'][0]))) + '\t',
            if op['name'] == 'lnorm':
                print '\t\t',
            else: 
                print ' '*4 + str(op['params'][1]) + '\t\t',
            print str(op['features'])
    
    
"""
Startup
"""
if __name__ == "__main__":
    main()