#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
Copy the first image from each subfolder of ../pf83aligned-whole and paste in the
same subfolder on ../pf83aligned-performance.
"""

import os
import shutil

inPath = '../pf83aligned-whole/'
outPath = '../pf83aligned-performance/'

# Clear outPath
for outFile in os.listdir(outPath):
    shutil.rmtree(os.path.join(outPath,outFile))

# Create folders and copy files
for inDir in os.listdir(inPath):
    inDirPath = os.path.join(inPath,inDir)
    if os.path.isdir(inDirPath):
        outDirPath = os.path.join(outPath,inDir)
        os.mkdir(outDirPath)
        for inImageFile in os.listdir(inDirPath):
            _,ext = os.path.splitext(inImageFile)
            if ext.lower() == '.jpg':
                shutil.copyfile(os.path.join(inDirPath,inImageFile), os.path.join(outDirPath,inImageFile))
                break