#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
import argparse
import os
import pickle
from pprint import pprint

import cnnmodels
import functions
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

dense_data = False

class PointBrowser:
    """
    Click on a point to select and highlight it -- the data that
    generated the point will be shown in the lower axes.  Use the 'n'
    and 'p' keys to browse through the next and previous points
    """
    def __init__(self,models,xs,ys,fig,ax,points,selPoints):
        self.lastind = 0
        
        self.lastSelPoint = None
        self.selPoints = selPoints
        self.models = models
        self.ax = ax
        self.points = points
        self.xs = xs
        self.ys = ys
        self.fig = fig

    def onpress(self, event):
        if self.lastind is None: return
        if event.key not in ('left', 'right'): return
        if event.key=='right': inc = 1
        else:  inc = -1
        
        self.leftClick = False
        self.rightClick = False
        
        self.lastind += inc
        self.lastind = np.clip(self.lastind, 0, len(self.xs)-1)
        self.update()

    def onpick(self, event):

        if event.artist!=self.points: return True

        N = len(event.ind)
        if not N: return True
       
        # the click locations
        x = event.mouseevent.xdata
        y = event.mouseevent.ydata
       
        self.leftClick = event.mouseevent.button == 1
        self.rightClick = event.mouseevent.button == 3

        distances = ((x-self.xs[event.ind])**2 + (y-self.ys[event.ind])**2)**0.5
        indmin = distances.argmin()
        dataind = event.ind[indmin]

        self.lastind = dataind
        self.update()

    def update(self):
        if self.lastind is None: return

        dataind = self.lastind
        
        if (self.leftClick):
            if self.lastSelPoint is None or self.lastSelPoint != self.selPoints[dataind]:
                if dense_data:
                    self.selPoints[dataind] = self.ax.plot(self.xs[dataind],self.ys[dataind],'or',markersize=12)
                else:
                    self.selPoints[dataind] = self.ax.plot(self.xs[dataind],self.ys[dataind],'or',markersize=15)
            
        if not self.lastSelPoint is None:
            self.lastSelPoint.pop(0).remove()
            self.lastSelPoint = None
            
        if (not self.rightClick):
            self.lastSelPoint = self.ax.plot(self.xs[dataind],self.ys[dataind],'og')
            
            print('Model index: ' + str(dataind))
            pprint(self.models[dataind])
            print('Time: ' + str(self.xs[dataind]) + ' - Accuracy: ' + str(self.ys[dataind]))
            print('Features dimensions: ' + \
            str(functions.featureShape(self.models[dataind])))
        
        if (self.rightClick):
            if (self.selPoints[dataind] != None):
                self.selPoints[dataind].pop(0).remove()
                self.selPoints[dataind] = None
        
        self.fig.canvas.draw()
        
def main():
    
    # Matplotlib options ---
    font = {
            'family' : 'sans',
            'weight' : 'normal',
            'size'   : 26}
    matplotlib.rc('font', **font)
    
    plt.rcParams['xtick.major.pad']='10'
    plt.rcParams['ytick.major.pad']='7'
    
    # Arguments parser ---
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("file", help="model search result file(s)",nargs="+")
    parser.add_argument("-r","--ref", help="reference accuracy level",nargs=1,type=float)
    args = parser.parse_args()

    
    filename1 = args.file[0]
    path = os.path.dirname(filename1)
    if len(args.file) > 1:
        filename2 = args.file[1]
       
    platform1 = os.path.basename(filename1).split("_")[0]
    platform2 = None
    result1 = pickle.load( open( filename1, "rb" ))
    if len(result1) == 5:
        merged = False
    elif len(result1) == 3:
        merged = True
    else:
        print("Unrecognized format " + filename1)
        return
        
    if merged:
        models, timings, accuracies = result1
    else:
        if filename2 != None:
            result2 = pickle.load( open( filename2, "rb" ))
            platform2 = os.path.basename(filename2).split("_")[0]
            parameters_labels, parameters_comb, timings, accuracies, base_model = \
                functions.modelSearchResultsMerge(result1,result2)
        else:
            parameters_labels, parameters_comb, timings, accuracies, base_model = \
                functions.modelSearchResultsMerge(result1,None)
        
        #Base model
        found = False;
        for candBaseModelName in list(cnnmodels.models.keys()):
            candBaseModel = cnnmodels.models[candBaseModelName]

            if (base_model == candBaseModel):
                found = True;
                break
                
        if not found:
            print('Base model not found')
            pprint(base_model)
            par_comb_matrix = np.array(parameters_comb)
            print('Parameters: ')
            for par_idx, par_label in enumerate(parameters_labels):
                print("('" + par_label + "', " + str(list(np.unique(par_comb_matrix[:,par_idx]))) + '),')
            return
        #Parameters
        par_comb_matrix = np.array(parameters_comb)
        print('Parameters: ')
        for par_idx, par_label in enumerate(parameters_labels):
            print("('" + par_label + "', " + str(list(np.unique(par_comb_matrix[:,par_idx]))) + '),')
        
        print('Base model: ' + candBaseModelName)
        pprint(base_model)
        
        #Model description
        models = []
        for par in parameters_comb:
            models += [functions.modelModify(base_model,parameters_labels,par)]
    
    timings = np.array(timings);
    accuracies = np.array(accuracies);
    
    num_data = timings.size
    global dense_data
    dense_data = num_data > 100
    
    # Plot ---
    fig = plt.figure(figsize=(10,6),dpi=72)
    fig.subplots_adjust(left=0.10,bottom=0.17,right=0.97,top=0.93)
    ax = plt.subplot(111)
    if dense_data:
        points, = ax.plot(timings, accuracies, 'o', markersize=6, picker=10)
    else:
        points, = ax.plot(timings, accuracies, 'o', markersize=12, picker=20)

    selPoints = len(models)*[None]
    
    browser = PointBrowser(models,timings,accuracies,fig,ax,points,selPoints)
    fig.canvas.mpl_connect('pick_event', browser.onpick)
    fig.canvas.mpl_connect('key_press_event', browser.onpress)
    
    if args.ref:
        ax.annotate('l3-1',
                    xy=(0, args.ref[0]), xycoords='data',
                    xytext=(-0.30, args.ref[0]), textcoords='data',
                    size=26,
            arrowprops=dict(frac=0.3,width=1,facecolor='black', shrink=0.1, headwidth=8),
            ha = 'center', va = 'center', rotation='vertical'
            )
        plt.axhline(args.ref[0],linestyle='--',color = 'k',linewidth=2.0)
    
    plt.xlabel('Feature extraction time [s]')
    plt.ylabel('Accuracy [%]')
    plt.grid(True)
    
    # Save plot ---
    if platform2:
        if platform1 < platform2:
            platform_prefix = platform1 + '-' + platform2
        else:
            platform_prefix = platform2 + '-' + platform1
    else:
        platform_prefix = platform1
    if merged:
        base_model_name = os.path.splitext(os.path.basename(filename1))[0].split('_')[1]
        savePath = os.path.join(path,platform_prefix+'_'+base_model_name + '.pdf')
    else:
        savePath = os.path.join(path,platform_prefix+'_'+candBaseModelName + '.pdf')
    
    print('Saving plot picture in ' + savePath)
    plt.savefig(savePath,transparent=True)
    
    plt.show()
    
    """
    Model Search Generator
    """
    activePoints = []
    for idx, point in enumerate(selPoints):
        if point != None:
            activePoints += [idx]
    
    if len(activePoints) > 1:
        common_model, par_labels, par_combs = functions.modelsCommonDiff(np.array(models)[activePoints]);
        
        print("Common base model") 
        pprint(common_model)
        
        print('Varying parameters: ')
        for par_idx, par_label in enumerate(par_labels):
            print("('" + par_label + "', " + str(par_combs[par_idx]) + '),')
    
if __name__ == "__main__":
    main();