#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
import functions as fct
import argparse
import numpy as np
import os
from atc_accuracy import generate_print_report

ACC_PATH = 'results/atc/accuracy/'

def main():
    """
    Arguments parser
    """
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("accuracy",help="path of the accuracy results file",nargs=2)
    priority_group = parser.add_mutually_exclusive_group()
    priority_group.add_argument("-p","--pca",help="merge pca components over common qsteps",action='store_true')
    priority_group.add_argument("-q","--qstep",help="merge qsteps over comment pca components",action='store_true')
    parser.add_argument("--suffix",help="result filename suffix")
    args = parser.parse_args()
    
    # Parse and compare platform, dataset, model from filenames
    filename1 = os.path.basename(args.accuracy[0])
    filename2 = os.path.basename(args.accuracy[1])
    
    platform1,dataset1,model1 = filename1.split('_')[:3]
    platform2,dataset2,model2 = filename2.split('_')[:3]
    
    assert platform1 == platform2
    assert dataset1 == dataset2
    assert model1 == model2
    
    output_filename = platform1 + '_' + dataset1 + '_' + model1
        
    # Load files ---
    res1 = fct.load(args.accuracy[0])
    res2 = fct.load(args.accuracy[1])
    
    acc1 = res1['accuracy'].astype(np.float32)
    acc2 = res2['accuracy'].astype(np.float32)
    
    pca1 = res1['pca'].astype(np.int16)
    pca2 = res2['pca'].astype(np.int16)
    
    levels1 = res1['level'].astype(np.float32)
    levels2 = res2['level'].astype(np.float32)
    
    size1 = res1['rate'].astype(np.float32)
    size2 = res2['rate'].astype(np.float32)
    
    qstep1 = res1['qstep'].astype(np.float32)
    qstep2 = res2['qstep'].astype(np.float32)
    
    dist1 = res1['snr'].astype(np.float32)
    dist2 = res2['snr'].astype(np.float32)
    
    pca_merge = False
    qstep_merge = False
    
    # Same qstep values -> PCA merge ---
    if np.array_equal(np.unique(qstep1),np.unique(qstep2)):
        print('PCA merge')
        pca_merge = True
        qstep_values = np.unique(qstep1)
        
    # Same PCA levels -> Qstep merge ---
    elif np.array_equal(np.unique(pca1),np.unique(pca2)):
        print('Qstep merge')
        qstep_merge = True
        pca_values = np.unique(pca1).astype(np.int16)
        
    else:
        # Determine if pca or qstep merge are possible
        pca_values_intersect = np.intersect1d(np.unique(pca1),np.unique(pca2),assume_unique = True)
        qstep_values_intersect = np.intersect1d(np.unique(qstep1),np.unique(qstep2),assume_unique = True)
        
        if pca_values_intersect.size > 0 and qstep_values_intersect.size > 0:
            print('Both PCA and Qstep partial merge are possible')
            if args.pca:
                print('PCA partial merge due to user request')
                pca_merge = True
                qstep_values = qstep_values_intersect
            elif args.qstep:
                print('Qstep partial merge due to user request')
                qstep_merge = True
                pca_values = pca_values_intersect
            else:
                if pca_values_intersect.size < qstep_values_intersect.size:
                    print('PCA partial merge due automatic selection')
                    pca_merge = True
                    qstep_values = qstep_values_intersect
                else:
                    print('Qstep partial merge due automatic selection')
                    qstep_merge = True
                    pca_values = pca_values_intersect
        elif pca_values_intersect.size > 0:
            print('Qstep partial merge')
            qstep_merge = True
            pca_values = pca_values_intersect
        elif qstep_values_intersect.size > 0:
            print('PCA partial merge')
            pca_merge = True
            qstep_values = qstep_values_intersect
        else:
            exit('Merge impossible')
        
    # Effective merge ---
    
    if pca_merge:
        # PCA partial merge ---
        assert qstep_values is not None
        
        pca_values = np.unique(np.concatenate((pca1,pca2))).astype(np.int16)
        
        num_comb =  qstep_values.size * pca_values.size
        
        pca_merge = np.empty(num_comb,dtype=np.int16)
        levels_merge = np.empty(num_comb,dtype=np.float32)
        acc_merge = np.empty(num_comb,dtype=np.float32)
        size_merge = np.empty(num_comb,dtype=np.float32)
        qstep_merge = np.empty(num_comb,dtype=np.float32)
        dist_merge = np.empty((num_comb,2),dtype=np.float32)
        
        idx_comb = 0
        for pca in pca_values:
            for qstep in qstep_values:
                idxs1 = np.bitwise_and(pca1 == pca,qstep1 == qstep)
                idxs2 = np.bitwise_and(pca2 == pca,qstep2 == qstep)
                
                assert idxs1.sum() == 0 or idxs1.sum() == 1
                assert idxs2.sum() == 0 or idxs2.sum() == 1
                assert idxs1.sum() + idxs2.sum() >= 1
                
                pca_merge[idx_comb] = pca
                qstep_merge[idx_comb] = qstep
                
                if idxs1.sum() == 1:
                    levels_merge[idx_comb] = levels1[idxs1]
                    acc_merge[idx_comb] = acc1[idxs1]
                    size_merge[idx_comb] = size1[idxs1]
                    dist_merge[idx_comb,:] = dist1[idxs1,:]
                else:
                    levels_merge[idx_comb] = levels2[idxs2]
                    acc_merge[idx_comb] = acc2[idxs2]
                    size_merge[idx_comb] = size2[idxs2]
                    dist_merge[idx_comb,:] = dist2[idxs2,:]
                
                idx_comb += 1
                
    elif qstep_merge:
        # Qstep merge ---
        
        assert pca_values is not None
        
        qstep_values = np.unique(np.concatenate((qstep1,qstep2)))
        
        num_comb =  qstep_values.size * pca_values.size
        
        pca_merge = np.empty(num_comb,dtype=np.int16)
        levels_merge = np.empty(num_comb,dtype=np.float32)
        acc_merge = np.empty(num_comb,dtype=np.float32)
        size_merge = np.empty(num_comb,dtype=np.float32)
        qstep_merge = np.empty(num_comb,dtype=np.float32)
        dist_merge = np.empty((num_comb,2),dtype=np.float32)
        
        idx_comb = 0
        for pca in pca_values:
            for qstep in qstep_values:
                idxs1 = np.bitwise_and(pca1 == pca,qstep1 == qstep)
                idxs2 = np.bitwise_and(pca2 == pca,qstep2 == qstep)
                
                assert idxs1.sum() == 0 or idxs1.sum() == 1
                assert idxs2.sum() == 0 or idxs2.sum() == 1
                assert idxs1.sum() + idxs2.sum() >= 1
                
                pca_merge[idx_comb] = pca
                qstep_merge[idx_comb] = qstep
                
                if idxs1.sum() == 1:
                    levels_merge[idx_comb] = levels1[idxs1]
                    acc_merge[idx_comb] = acc1[idxs1]
                    size_merge[idx_comb] = size1[idxs1]
                    dist_merge[idx_comb,:] = dist1[idxs1,:]
                else:
                    levels_merge[idx_comb] = levels2[idxs2]
                    acc_merge[idx_comb] = acc2[idxs2]
                    size_merge[idx_comb] = size2[idxs2]
                    dist_merge[idx_comb,:] = dist2[idxs2,:]
                
                idx_comb += 1
        
    else:
        exit('Error occurred')
                            
    output_filename += '_pca-'+'-'.join(map(str,pca_values))
    output_filename += '_qstep-'+'-'.join(map(str,qstep_values))
    if args.suffix: output_filename += '_' + args.suffix
    
    output_path = os.path.join(ACC_PATH,output_filename+'.pickle')
    
    print("Saving merged accuracy in " + output_path)
    res_out = {'accuracy':acc_merge,
              'pca':pca_merge,
              'level':levels_merge,
              'rate':size_merge,
              'qstep':qstep_merge,
              'snr':dist_merge}
    fct.save(res_out, output_path);
    
    filenameTxt = os.path.splitext(output_path)[0] + '.txt'
    print("Saving merged accuracy report in " + filenameTxt)
    generate_print_report(res_out,filenameTxt)
        
if __name__ == "__main__":
    main();