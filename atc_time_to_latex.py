#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
- Timings loading
- Statistics
"""
import numpy as np
import pickle
import os
import cnnmodels
import argparse

RESULTS_PATH = 'results/atc/time/'

def main():
    """
    Arguments parser
    """
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("infile",help="timing input file")
    
    args = parser.parse_args()
    infile = args.infile
    
    """
    Check if input file exists
    """
    if not os.path.isfile(infile):
        exit("Input file " + infile + " does not exist")
    
    filenamePrefix = os.path.basename(infile)
    if (filenamePrefix.endswith('.pickle')):
        filenamePrefix = filenamePrefix.split('.pickle')[0]

    outLatexName = filenamePrefix + '.tex'
    
    modelName = filenamePrefix.split("_")[2]
    model = cnnmodels.models[modelName]

    """
    Checking directory structure
    """
    if not os.path.isdir(RESULTS_PATH):
        os.mkdir(RESULTS_PATH)

    """
    Timings loading
    """
    print("Loading timings...")
    timings = pickle.load( open( infile, "rb" ))
    
    numImgs = len(timings)
    numLayers = len(model['description'])
    
    numOps = 0
    for layer in model['description']:
        numOps += len(layer)
    
    """
    Mean time for each operation
    """
    opsTime = np.zeros(numOps)
    for imgTime in timings:
        opsIdx = 0
        for layer in imgTime[1]:
            for op in layer[1]:
                opsTime[opsIdx] += op[0]
                opsIdx += 1
    opsTime /= numImgs
    
    
    """
    Mean time for each layer
    """
    layersTime = np.zeros(numLayers)
    for imgTime in timings:
        for layerIdx,layer in enumerate(imgTime[1]):
            layersTime[layerIdx] += layer[0]
    layersTime /= numImgs
    
    """
    Mean overall time
    """
    totTime = 0
    for imgTime in timings:
        totTime += imgTime[0]
    totTime /= numImgs
    
    """
    Preparing table
    """
    table = "\\begin{tabular}{|l|c|c|} \n"
    table += "\\hline \n "
    table += " & \\textbf{Time [s]} & \\textbf{\\%} \\\\ \n"
    
    operationIdx = 0
    for layerIdx,layer in enumerate(model['description']):
        table += "\\hline \n "
        table += "\\textbf{Layer %d} & %05.2f & %05.2f \\\\ \n" %(layerIdx,layersTime[layerIdx],layersTime[layerIdx]/totTime*100)
        
        for operation in layer:
            table += operation[0] + " & %05.2f & %05.2f \\\\ \n" % (opsTime[operationIdx],opsTime[operationIdx]/totTime*100)
            operationIdx += 1
    
    table += "\\hline \n "
    table += "\\textbf{\\textit{Tot}} & %05.2f &  %05.2f \\\\ \n" %(totTime,100)
    table += "\\hline \n "
    table += "\\end{tabular}"
    
    """
    Printing and saving
    """
    print(table)
    
    outFile = open(os.path.join(RESULTS_PATH,outLatexName),'w')
    outFile.write(table)
    outFile.close()
    
    print("Results stored in " + os.path.join(RESULTS_PATH,outLatexName))

if __name__ == "__main__":
    main();