#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
Dependencies test. Check Python modules availability and version.
- Cython (0.21)
- numpy (1.8.1)
- numexpr (2.4)
- scipy (0.14.0)
- six (1.8.0)
- skimage (0.9.3)
- PIL (1.1.7)
- sklearn (0.15.2)
--- Not needed on sensor nodes ---
- joblib (0.7.1)
- matplotlib (1.4.0)
"""

"""
Compare two version strings.
@param aVer
@param bVer
@return -1 if aVer < bVer, 0 if aVer == bVer, 1 if aVer > bVer
"""
def versionCompare(aVer,bVer):
    assert type(aVer) == type('') and type(bVer) == type('')
    
    aList = aVer.split('.')
    bList = bVer.split('.')
    
    for idx in range(max(len(aList),len(bList))):
        try:
            aVal = int(aList[idx]);
        except ValueError:
            aSubList = aList[idx].split('-')
            aVal = int(aSubList[0])
        except IndexError:
            if (int(bList[idx]) > 0):
                    return -1
               
        try:
            bVal = int(bList[idx]);
        except ValueError:
            bSubList = bList[idx].split('-')
            bVal = int(bSubList[0])
        except IndexError:
            if (int(aList[idx]) > 0):
                    return 1
        
        if (aVal < bVal):
            return -1
        if (aVal > bVal):
            return 1
                
    return 0

def main():

    errors = False
    warnings = False
    
    try:
        import Cython
        if versionCompare(Cython.__version__, '0.21') < 0:
            warnings = True
            print(('Cython detected version ' + Cython.__version__ + ', minimum expected 0.21'))
        else:
            print(('Cython version: ' + Cython.__version__))
    except ImportError:
        errors = True
        print('Cython module not found')
        
    try:
        import numpy
        if versionCompare(numpy.__version__, '1.8.1') < 0:
            warnings = True
            print(('numpy detected version ' + numpy.__version__ + ', minimum expected 1.8.1'))
        else:
            print(('numpy version: ' + numpy.__version__))
            print('numpy BLAS: ')
            numpy.show_config()
    except ImportError:
        errors = True
        print('numpy module not found')
        
    try:
        import scipy
        if versionCompare(scipy.__version__, '0.14.0') < 0:
            warnings = True
            print(('scipy detected version ' + scipy.__version__ + ', minimum expected 0.14.0'))
        else:
            print(('scipy version: ' + scipy.__version__))
            print('scipy BLAS: ')
            scipy.show_config()
    except ImportError:
        errors = True
        print('scipy module not found')
    
    try:
        import numexpr
        if versionCompare(numexpr.__version__, '2.4') < 0:
            warnings = True
            print(('numexpr detected version ' + numexpr.__version__ + ', minimum expected 2.4'))
        else:
            print(('numexpr version: ' + numexpr.__version__))
    except ImportError:
        errors = True
        print('numexpr module not found')
        
    try:
        import six
        if versionCompare(six.__version__, '1.8.0') < 0:
            warnings = True
            print(('six detected version ' + six.__version__ + ', minimum expected 1.8.0'))
        else:
            print(('six version: ' + six.__version__))
    except ImportError:
        errors = True
        print('six module not found')
    
    try:
        import skimage
        if versionCompare(skimage.__version__, '0.9.3') < 0:
            warnings = True
            print(('skimage detected version ' + skimage.__version__ + ', minimum expected 0.9.3'))
        else:
            print(('skimage version: ' + skimage.__version__))
    except ImportError:
        errors = True
        print('skimage module not found')
    
    try:
        import PIL
        if versionCompare(PIL.VERSION, '1.1.7') < 0:
            warnings = True
            print(('PIL detected version ' + PIL.VERSION + ', minimum expected 1.1.7'))
        else:
            print(('PIL version: ' + PIL.VERSION))
    except ImportError:
        errors = True
        print('PIL module not found')
        
    print('\n--- Not needed on sensor nodes ---')
    
    try:
        import sklearn
        if versionCompare(sklearn.__version__, '0.15.2') < 0:
            warnings = True
            print(('sklearn detected version ' + sklearn.__version__ + ', minimum expected 0.15.2'))
        else:
            print(('sklearn version: ' + sklearn.__version__))
    except ImportError:
        errors = True
        print('sklearn module not found')
    
    try:
        import joblib
        if versionCompare(joblib.__version__, '0.7.1') < 0:
            warnings = True
            print(('joblib detected version ' + joblib.__version__ + ', minimum expected 0.7.1'))
        else:
            print(('joblib version: ' + joblib.__version__))
    except ImportError:
        errors = True
        print('joblib module not found')
        
        
    try:
        import matplotlib
        version = str(matplotlib.__version__)
        if versionCompare(version, '1.4.0') < 0:
            warnings = True
            print(('matplotlib detected version ' + version + ', minimum expected 1.4.0'))
        else:
            print(('matplotlib version: ' + version))
    except ImportError:
        errors = True
        print('matplotlib module not found')
    
    if not errors:
        print('Dependencies OK')
        if warnings:
            print('Please check versions')
        
if __name__ == '__main__':
    main()