#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
CTA: Image compression and features extraction with CNN
"""

import time
import argparse
import numpy as np
from PIL import Image
import os
import random
from scipy import misc

import data
import cnnrandom as cnn
import cnnmodels
import functions as fct

TIME_PATH = 'results/cta/time/'
FEAT_PATH = 'results/cta/features/'
SIZE_PATH = 'results/cta/size/'

"""
Features and image size
"""
def imageFeatureSize(featuresExtractor, imgPath, imageShape, jpegQuality,imgIdx=None,imgsNum=None,t0=None):
    
    # Load the image
    img = Image.open(imgPath).convert('L')
    
    # image compression and size measurement
    filename = '.' + ''.join(random.choice('0123456789ABCDEF') for _ in range(16)) + '.jpg'
    img.save(filename,quality=jpegQuality)
    imageSize = os.stat(filename).st_size*8
    img = misc.imread(filename, flatten=True)
    os.unlink(filename)
    
    # Image resize
    img = misc.imresize(img, imageShape).astype('float32')
    
    # image scaling between 0 and 1
    img -= img.min()
    img /= img.max()
    
    # features extraction
    img.shape = (1,) + img.shape
    imageFeatures, _ =  featuresExtractor.extract(arr_in=img)
    
    if imgIdx is not None: 
        # timing informations
        t1 = time.time()
        print('Image %05d/%05d'%(imgIdx+1,imgsNum) + ' - QF ETA ' + \
        time.strftime('%H:%M', time.gmtime((t1 - t0) / (imgIdx + 1) * (imgsNum - imgIdx - 1) + time.time() - time.altzone)))
    
    return imageFeatures, imageSize

def main():

    """
    Arguments parser
    """
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("datasetPath",help="path of the image dataset")
    parser.add_argument("model",help="one of the following CNN models: " \
                        + ' '.join(sorted(cnnmodels.models.keys())))
    parser.add_argument("platformPrefix",help="platform prefix used as output filename prefix")
    parser.add_argument("-qf","--quality-factor",help="JPEG quality factor",required=True,nargs="+",type=int)
    parser.add_argument("-m","--multithread",help="number of threads used to perform feature extraction",type=int,nargs=1)
    group = parser.add_mutually_exclusive_group()
    group.required = True
    group.add_argument("-t","--timing",help="perform time profiling and size measurement",action="store_true")
    group.add_argument("-f","--features",help="perform features extraction and size measurement",action="store_true")
    
    args = parser.parse_args()
    
    datasetPath = args.datasetPath
    datasetName = datasetPath.rsplit('/')[-1]
    if len(datasetName) == 0:
        datasetName = datasetPath.rsplit('/')[-2]
        
    baseFilename = args.platformPrefix + '_' + datasetName + '_' + args.model
    
    """
    CNN initialization
    """
    modelName = args.model
    #baseFilename += '_' + modelName
        
    if not fct.checkModel(modelName):
        exit();
    
    model = cnnmodels.models[modelName]
    modelDescr = model['description']
    modelParams = model['input-params']

    image_shape = modelParams['imageSize']
    
    """
    Images list
    """
    dataObj = data.cvprw11_setup(datasetPath)
    imgsPath = dataObj.classification_task()[0]
    imgsNum = len(imgsPath)

    if args.features:
        
        fct.createDir(SIZE_PATH)
        fct.createDir(FEAT_PATH)
        
        threads_num = False
        if args.multithread:
            threads_num = args.multithread[0]
        
        if threads_num:
            try:
                import multiprocessing
                if multiprocessing.cpu_count() > 1:
                    from joblib import Parallel, delayed
                else:
                    threads_num = False
            except ImportError:
                threads_num = False
    
        featuresShape = fct.featureShape(model)
        featuresExtractor = cnn.BatchExtractor(image_shape, modelDescr)

        """
        Images cycle - Features extraction and accuracy evaluation
        """
        qf_num = len(args.quality_factor)
        fileSize = np.empty((qf_num,imgsNum))
        t0 = time.time()
        for qf_idx, qf in enumerate(args.quality_factor):
            print('JPEG QF: ' + str(qf))
            
            print("Images features extraction")
            t0_qf = time.time()
            if threads_num != False:
                print("Parallelized extraction with " + str(threads_num) + " threads")
                
                results = Parallel(n_jobs=threads_num)(
                    delayed(imageFeatureSize)(
                        featuresExtractor,
                        imgPath,
                        image_shape,
                        qf,
                        imgIdx,
                        imgsNum, 
                        t0_qf)
                    for imgIdx, imgPath in enumerate(imgsPath))

                features = np.empty((imgsNum,) + featuresShape,dtype=np.float32)
                sizes = np.empty((imgsNum,),dtype=np.float32)
                
                for idx, result in enumerate(results):
                    features[idx,:,:,:] = result[0]
                    sizes[idx] = result[1]
                
                features = np.array(features,dtype=np.float32)
                fileSize[qf_idx,:] = np.array(sizes,dtype=np.float32)
                
            else:
                features = np.empty((imgsNum,) + featuresShape, dtype=np.float32)
                for imgIdx, imgPath in enumerate(imgsPath):
                    features[imgIdx], fileSize[qf_idx,imgIdx] = imageFeatureSize(featuresExtractor, imgPath, image_shape, qf, imgIdx, imgsNum, t0_qf)
            
            assert features.dtype == np.float32
            assert features.shape == (imgsNum,) + featuresShape
            
            features_file = os.path.join(FEAT_PATH,baseFilename + '_qf-' + str(qf) + '.npy');
            
            print('Saving features in ' + features_file)
            np.save(features_file,features)
    
            print('JPEG quality ' + str(qf) + ' file size ' + str(np.mean(fileSize[qf_idx,:])) + ' bit')
            
            t1 = time.time()
            print('Global ETA ' + \
            time.strftime('%H:%M', time.gmtime((t1 - t0) / (qf_idx + 1) * (qf_num - qf_idx - 1) + time.time() - time.altzone)))
    
            
        print('\n--------\n')
        print('Total compression and extraction time: ' + str(time.time() - t0) + 's')
        data_file = os.path.join(FEAT_PATH,baseFilename + '.pickle');
        print('Saving data in ' + data_file)
        # generate splits so to have them saved
        try:
            dataObj.classification_splits
        except AssertionError:
            print('Dataset not suitable for 10 fold cross validation with PubFig83 protocol')
        fct.save(dataObj,data_file)
        size_file = os.path.join(SIZE_PATH,baseFilename+'.pickle')
        print('Size saved in ' + size_file)
        fct.save({'quality':args.quality_factor,'size':np.mean(fileSize,axis=1)},size_file);
    
    if args.timing:
        
        fct.createDir(TIME_PATH)
        
        timings = np.empty((len(args.quality_factor),imgsNum))
        fileSize = np.empty((len(args.quality_factor),imgsNum))
        
        t0 = time.time()
        qf_num = len(args.quality_factor)
        for qf_idx, qf in enumerate(args.quality_factor):
            print('--- JPEG quality: ' + str(qf) + ' ---')
            for imgIdx, imgPath in enumerate(imgsPath):
                
                # image compression and size measurement
                # QF=10, performance database, mean size = 9821 bit 
                #img = Image.open(imgPath).convert('F').convert('RGB')
                # QF=10, performance database, mean size = 6896 bit 
                img = Image.open(imgPath).convert('L')
                # QF=10, performance database, mean size = 7062 bit
                #img = misc.toimage(misc.imread(imgPath,flatten=True))
                t0 = time.time()
                img.save(TIME_PATH+'temp.jpg','JPEG',quality=qf)
                img = misc.imread(TIME_PATH+'temp.jpg',flatten=True)
                t1 = time.time()
                timings[qf_idx,imgIdx] = t1-t0
                fileSize[qf_idx,imgIdx] = os.stat(TIME_PATH+'temp.jpg').st_size*8
            
            print('JPEG quality ' + str(qf) + ' compression time ' + str(np.mean(timings[qf_idx,:])) + ' s')
            print('JPEG quality ' + str(qf) + ' file size ' + str(np.mean(fileSize[qf_idx,:])) + ' bit')
            
            t1 = time.time()
            print('ETA ' + \
            time.strftime('%H:%M', time.gmtime((t1 - t0) / (qf_idx + 1) * (qf_num - qf_idx - 1) + time.time() - time.altzone)))
    
    
        os.unlink(TIME_PATH+'temp.jpg')
        
        print('--- ---')
        saveFileName = os.path.join(TIME_PATH,baseFilename+'.pickle')
        print('Time and size saved in ' + saveFileName)
        fct.save({'quality':args.quality_factor,'time':np.mean(timings,axis=1),'size':np.mean(fileSize,axis=1)},saveFileName);

if __name__ == "__main__":
    main();