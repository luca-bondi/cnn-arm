models = {
    # original model 
    'fg11-ht-l3-1' : {
        'input-params':{
            'imageSize' : (200, 200),
        },
        'description':[
            [('lnorm',
              {'kwargs': {'inker_shape': [9, 9],
                          'outker_shape': [9, 9],
                          'remove_mean': False,
                          'stretch': 10,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (3, 3),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 64,
                             },
               'kwargs': {'max_out': None,
                           'min_out': 0,
                          }}),
             ('lpool', {'kwargs': {'ker_shape': [7, 7], 'order': 1, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [5, 5],
                          'outker_shape': [5, 5],
                          'remove_mean': False,
                          'stretch': 0.1,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (5, 5),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 128,
                             },
               'kwargs': {'max_out': None,
                          'min_out': 0,
                         }}),
             ('lpool', {'kwargs': {'ker_shape': [5, 5], 'order': 1, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [7, 7],
                          'outker_shape': [7, 7],
                          'remove_mean': False,
                          'stretch': 1,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (5, 5),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 256,
                              },
               'kwargs': {'max_out': None,
                          'min_out': 0,
                         }}),
             ('lpool', {'kwargs': {'ker_shape': [7, 7], 'order': 10, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [3, 3],
                          'outker_shape': [3, 3],
                          'remove_mean': False,
                          'stretch': 10,
                          'threshold': 1}})]
        ]
    },
    # original model with upper thresholding
    'fg11-ht-l3-1-th' : {
        'input-params':{
            'imageSize' : (200, 200),
        },
        'description':[
            [('lnorm',
              {'kwargs': {'inker_shape': [9, 9],
                          'outker_shape': [9, 9],
                          'remove_mean': False,
                          'stretch': 10,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (3, 3),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 64,
                             },
               'kwargs': {'max_out': 1,
                           'min_out': 0,
                          }}),
             ('lpool', {'kwargs': {'ker_shape': [7, 7], 'order': 1, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [5, 5],
                          'outker_shape': [5, 5],
                          'remove_mean': False,
                          'stretch': 0.1,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (5, 5),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 128,
                             },
               'kwargs': {'max_out': 1,
                          'min_out': 0,
                         }}),
             ('lpool', {'kwargs': {'ker_shape': [5, 5], 'order': 1, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [7, 7],
                          'outker_shape': [7, 7],
                          'remove_mean': False,
                          'stretch': 1,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (5, 5),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 256,
                              },
               'kwargs': {'max_out': 1,
                          'min_out': 0,
                         }}),
             ('lpool', {'kwargs': {'ker_shape': [7, 7], 'order': 10, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [3, 3],
                          'outker_shape': [3, 3],
                          'remove_mean': False,
                          'stretch': 10,
                          'threshold': 1}})]
        ]
    },
    # original model without upsampling and third layer
    'fg11-ht-l2-s1' : {
        'input-params':{
            'imageSize' : (100, 100),
        },
        'description':[
            [('lnorm',
              {'kwargs': {'inker_shape': [9, 9],
                          'outker_shape': [9, 9],
                          'remove_mean': False,
                          'stretch': 10,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (3, 3),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 64,
                             },
               'kwargs': {'max_out': None,
                           'min_out': 0,
                          }}),
             ('lpool', {'kwargs': {'ker_shape': [7, 7], 'order': 1, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [5, 5],
                          'outker_shape': [5, 5],
                          'remove_mean': False,
                          'stretch': 0.1,
                          'threshold': 1}})],
            [('fbcorr',
              {'initialize': {'filter_shape': (5, 5),
                              'generate': ('random:uniform', {'rseed': 42}),
                              'n_filters': 128,
                             },
               'kwargs': {'max_out': None,
                          'min_out': 0,
                         }}),
             ('lpool', {'kwargs': {'ker_shape': [5, 5], 'order': 1, 'stride': 2}}),
             ('lnorm',
              {'kwargs': {'inker_shape': [7, 7],
                          'outker_shape': [7, 7],
                          'remove_mean': False,
                          'stretch': 1,
                          'threshold': 1}})],
        ]
    },
    # optimal common model from fg11-ht-l2-s1 with L1_filt_num [16,32]
    'fg11-ht-l2-s2':{
        'input-params': {'imageSize': (100, 100)},
        'description': [[('lnorm',
                   {'kwargs': {'inker_shape': [9, 9],
                               'outker_shape': [9, 9],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (3, 3),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 16,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [7, 7],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [5, 5],
                               'outker_shape': [5, 5],
                               'remove_mean': False,
                               'stretch': 0.1,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (5, 5),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 128,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [7, 7],
                               'outker_shape': [7, 7],
                               'remove_mean': False,
                               'stretch': 1,
                               'threshold': 1}})]]
    },
    # model base for search 3: L1_filt_num 16
    'fg11-ht-l2-s3':
    {'description': [[('lnorm',
                   {'kwargs': {'inker_shape': [9, 9],
                               'outker_shape': [9, 9],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (3, 3),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 16,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [7, 7],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [5, 5],
                               'outker_shape': [5, 5],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (5, 5),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 128,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [7, 7],
                               'outker_shape': [7, 7],
                               'remove_mean': False,
                               'stretch': 1,
                               'threshold': 1}})]],
    'input-params': {'imageSize': (100, 100)}
    },
    # model base for search 4: L1_filt_num 32
    'fg11-ht-l2-s4':
    {'description': [[('lnorm',
                   {'kwargs': {'inker_shape': [9, 9],
                               'outker_shape': [9, 9],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (3, 3),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 32,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [7, 7],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [5, 5],
                               'outker_shape': [5, 5],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (5, 5),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 128,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [7, 7],
                               'outker_shape': [7, 7],
                               'remove_mean': False,
                               'stretch': 1,
                               'threshold': 1}})]],
    'input-params': {'imageSize': (100, 100)}
    },
    # model base for search 5: norm shape
    'fg11-ht-l2-s5':
    {'description':[[('lnorm',
                   {'kwargs': {'inker_shape': [3, 3],
                               'outker_shape': [3, 3],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (3, 3),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 16,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [5, 5],
                               'outker_shape': [5, 5],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (5, 5),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 128,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 10,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [7, 7],
                               'outker_shape': [7, 7],
                               'remove_mean': False,
                               'stretch': 1,
                               'threshold': 1}})]],
 'input-params': {'imageSize': (100, 100)}},
    # optimal model
    # Time: 1.06876 - Accuracy: 86.7349
    # Features dimensions: (14, 14, 128)
    'fg11-ht-l2-opt':
    {'description': [[('lnorm',
                   {'kwargs': {'inker_shape': [9, 9],
                               'outker_shape': [9, 9],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (3, 3),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 16,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [5, 5],
                               'outker_shape': [5, 5],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (5, 5),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 128,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 10,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [3, 3],
                               'outker_shape': [3, 3],
                               'remove_mean': False,
                               'stretch': 1,
                               'threshold': 1}})]],
 'input-params': {'imageSize': (100, 100)}},
    # Test model
    'test-l2':
    {'description': [[('lnorm',
                   {'kwargs': {'inker_shape': [9, 9],
                               'outker_shape': [9, 9],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (3, 3),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 16,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 1,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [5, 5],
                               'outker_shape': [5, 5],
                               'remove_mean': False,
                               'stretch': 10,
                               'threshold': 1}})],
                 [('fbcorr',
                   {'initialize': {
                                   'filter_shape': (5, 5),
                                   'generate': ('random:uniform',
                                                {'rseed': 42}),
                                   'n_filters': 16,
                                   },
                    'kwargs': {
                               'max_out': None,
                               'min_out': 0,
                               }}),
                  ('lpool',
                   {'kwargs': {'ker_shape': [5, 5],
                               'order': 10,
                               'stride': 2}}),
                  ('lnorm',
                   {'kwargs': {'inker_shape': [3, 3],
                               'outker_shape': [3, 3],
                               'remove_mean': False,
                               'stretch': 1,
                               'threshold': 1}})]],
 'input-params': {'imageSize': (100, 100)}},
          
}

# the key is always a model key
model_search = {
    # first search. Filters number
    'fg11-ht-l2-s1': [
        # ('L0_norm_shape' , [3,5,7,9]), #3,5,7,9
        # ('L0_norm_stretch' , [0.1,1,10]), #0.1,1,10
        # ('L0_norm_th' , [0.1,1,10]), #0.1,1,10

        # ('L1_filt_shape' , [3,5,7,9]), #3,5,7,9
        ('L1_filt_num' , [16, 32, 64]),  # 16,32,64
        # ('L1_act_min', [None, 0]), #None, 0
        # ('L1_act_max', [None, 1]), #None, 1
        # ('L1_pool_shape' , [3,5,7,9]), #3,5,7,9
        # ('L1_pool_exp' , [1,2,10]), #1,2,10
        # ('L1_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L1_norm_stretch' , [0.1,1,10]),#0.1,1,10
        # ('L1_norm_th' , [0.1,1,10]),#0.1,1,10

        # ('L2_filt_shape' , [3,5,7,9]), #3,5,7,9
        ('L2_filt_num' , [16, 32, 64, 128]),  # 16,32,64,128
        # ('L2_act_min', [None,0]),#None,0
        # ('L2_act_max', [None,1]),#None,1
        # ('L2_pool_shape' , [3,5,7,9]),#3,5,7,9
        # ('L2_pool_exp' , [1,2,10]),#1,2,10
        # ('L2_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L2_norm_stretch' , [0.1,1,10]),#0.1,1,10
        # ('L2_norm_th' , [0.1]),#0.1,1,10
    ],
    # second search. norm stretch
    'fg11-ht-l2-s2': [
        # ('L0_norm_shape' , [3,5,7,9]), #3,5,7,9
        ('L0_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L0_norm_th' , [0.1,1,10]), #0.1,1,10

        # ('L1_filt_shape' , [3,5,7,9]), #3,5,7,9
        ('L1_filt_num' , [16, 32]),  # 16,32,64
        # ('L1_act_min', [None, 0]), #None, 0
        # ('L1_act_max', [None, 1]), #None, 1
        # ('L1_pool_shape' , [3,5,7,9]), #3,5,7,9
        # ('L1_pool_exp' , [1,2,10]), #1,2,10
        # ('L1_norm_shape' , [3,5,7,9]),#3,5,7,9
        ('L1_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L1_norm_th' , [0.1,1,10]),#0.1,1,10

        # ('L2_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L2_filt_num' , [16,32,64,128]),#16,32,64,128
        # ('L2_act_min', [None,0]),#None,0
        # ('L2_act_max', [None,1]),#None,1
        # ('L2_pool_shape' , [3,5,7,9]),#3,5,7,9
        # ('L2_pool_exp' , [1,2,10]),#1,2,10
        # ('L2_norm_shape' , [3,5,7,9]),#3,5,7,9
        ('L2_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L2_norm_th' , [0.1]),#0.1,1,10
    ],
    # third search: pooling params with L1_filt_num = 16
    'fg11-ht-l2-s3': [
        # ('L0_norm_shape' , [3,5,7,9]), #3,5,7,9
        # ('L0_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L0_norm_th' , [0.1,1,10]), #0.1,1,10

        # ('L1_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L1_filt_num' , [16, 32]),  # 16,32,64
        # ('L1_act_min', [None, 0]), #None, 0
        # ('L1_act_max', [None, 1]), #None, 1
        ('L1_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        ('L1_pool_exp' , [1, 2, 10]),  # 1,2,10
        # ('L1_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L1_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L1_norm_th' , [0.1,1,10]),#0.1,1,10

        # ('L2_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L2_filt_num' , [16,32,64,128]),#16,32,64,128
        # ('L2_act_min', [None,0]),#None,0
        # ('L2_act_max', [None,1]),#None,1
        ('L2_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        ('L2_pool_exp' , [1, 2, 10]),  # 1,2,10
        # ('L2_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L2_norm_stretch' , [0.1, 1]),  # 0.1,1,10
        # ('L2_norm_th' , [0.1]),#0.1,1,10
    ],
    # fourth search: pooling params with L1_filt_num = 32
    'fg11-ht-l2-s4': [
        # ('L0_norm_shape' , [3,5,7,9]), #3,5,7,9
        # ('L0_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L0_norm_th' , [0.1,1,10]), #0.1,1,10

        # ('L1_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L1_filt_num' , [16, 32]),  # 16,32,64
        # ('L1_act_min', [None, 0]), #None, 0
        # ('L1_act_max', [None, 1]), #None, 1
        ('L1_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        ('L1_pool_exp' , [1, 2, 10]),  # 1,2,10
        # ('L1_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L1_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L1_norm_th' , [0.1,1,10]),#0.1,1,10

        # ('L2_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L2_filt_num' , [16,32,64,128]),#16,32,64,128
        # ('L2_act_min', [None,0]),#None,0
        # ('L2_act_max', [None,1]),#None,1
        ('L2_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        ('L2_pool_exp' , [1, 2, 10]),  # 1,2,10
        # ('L2_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L2_norm_stretch' , [0.1, 1]),  # 0.1,1,10
        # ('L2_norm_th' , [0.1]),#0.1,1,10
    ],
    # fifth search: norm shape params
    'fg11-ht-l2-s5': [
        ('L0_norm_shape' , [3,5,7,9]), #3,5,7,9
        # ('L0_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L0_norm_th' , [0.1,1,10]), #0.1,1,10

        # ('L1_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L1_filt_num' , [16, 32]),  # 16,32,64
        # ('L1_act_min', [None, 0]), #None, 0
        # ('L1_act_max', [None, 1]), #None, 1
        # ('L1_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        # ('L1_pool_exp' , [1, 2, 10]),  # 1,2,10
        ('L1_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L1_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L1_norm_th' , [0.1,1,10]),#0.1,1,10

        # ('L2_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L2_filt_num' , [16,32,64,128]),#16,32,64,128
        # ('L2_act_min', [None,0]),#None,0
        # ('L2_act_max', [None,1]),#None,1
        # ('L2_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        # ('L2_pool_exp' , [1, 2, 10]),  # 1,2,10
        ('L2_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L2_norm_stretch' , [0.1, 1]),  # 0.1,1,10
        # ('L2_norm_th' , [0.1]),#0.1,1,10
    ],
    'test-l2': [
        #('L0_norm_shape' , [3,5,7,9]), #3,5,7,9
        # ('L0_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L0_norm_th' , [0.1,1,10]), #0.1,1,10

        # ('L1_filt_shape' , [3,5,7,9]), #3,5,7,9
        # ('L1_filt_num' , [16, 32]),  # 16,32,64
        # ('L1_act_min', [None, 0]), #None, 0
        # ('L1_act_max', [None, 1]), #None, 1
        # ('L1_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        # ('L1_pool_exp' , [1, 2, 10]),  # 1,2,10
        #('L1_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L1_norm_stretch' , [0.1, 1, 10]),  # 0.1,1,10
        # ('L1_norm_th' , [0.1,1,10]),#0.1,1,10

        # ('L2_filt_shape' , [3,5,7,9]), #3,5,7,9
         ('L2_filt_num' , [16,32]),#16,32,64,128
        # ('L2_act_min', [None,0]),#None,0
        # ('L2_act_max', [None,1]),#None,1
        # ('L2_pool_shape' , [3, 5, 7, 9]),  # 3,5,7,9
        # ('L2_pool_exp' , [1, 2, 10]),  # 1,2,10
        #('L2_norm_shape' , [3,5,7,9]),#3,5,7,9
        # ('L2_norm_stretch' , [0.1, 1]),  # 0.1,1,10
        # ('L2_norm_th' , [0.1]),#0.1,1,10
    ],
}
