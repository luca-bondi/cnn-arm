#!/bin/bash

# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD

./atc_time_to_latex.py results/atc/time/rpi_pf83aligned-performance_fg11-ht-l2-opt.pickle
./atc_time_to_latex.py results/atc/time/rpi_pf83aligned-performance_fg11-ht-l3-1.pickle
./model_to_latex.py fg11-ht-l2-opt