#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
ATC: Image features extraction and time profiling with CNN 
"""
import argparse
import time
from scipy import misc
import numpy as np
import os

import cnnmodels
import cnnrandom as cnn
import data
import functions as fct

TIME_PATH = 'results/atc/time/'
FEAT_PATH = 'results/atc/features/'

BATCH_SIZE_DEFAULT = 100

"""
Extract image features and timing
@param path path of the image
@param shape (width,height)
"""
def imageExtract(featuresExtractor,path,shape,imgIdx=None,imgsNum=None,t0=None):
    
    assert len(shape) == 2 
    
    # Image loading and resizing 
    img = misc.imread(path, flatten=True)
    img = misc.imresize(img, shape).astype(np.float32)
    
    # image scaling between 0 and 1
    img -= img.min()
    img /= img.max()
    
    # features extraction
    img.shape = (1,) + img.shape
    results = featuresExtractor.extract(img)
    
    if imgIdx is not None: 
        # timing informations
        t1 = time.time()
        print('Image %5d of %5d' % (imgIdx + 1, imgsNum) + ' - ETA: ' + \
        time.strftime('%H:%M', time.gmtime((t1 - t0) / (imgIdx + 1) * (imgsNum - imgIdx - 1) + time.time() - time.altzone)))
        
    return results

def image_set_extract(featuresExtractor,path_list,shape,first_img_idx=None,imgs_num=None,t0=None):
    
    assert len(shape) == 2 
    
    # Image loading and resizing
    imgs = np.empty(((len(path_list),)+shape),dtype=np.float32) 
    for path_idx, path in enumerate(path_list):
        img = misc.imread(path, flatten=True)
        img = misc.imresize(img, shape).astype(np.float32)
    
        # image scaling between 0 and 1
        img -= img.min()
        img /= img.max()
        
        imgs[path_idx,:,:] = img
    
    last_img_idx = first_img_idx + len(path_list)
    
    # features extraction
    results = featuresExtractor.extract(imgs)
    
    if first_img_idx is not None: 
        # timing informations
        t1 = time.time()
        print('Images block from %5d to %5d of %5d' % (first_img_idx + 1, last_img_idx, imgs_num) + ' - ETA: ' + \
        time.strftime('%H:%M', time.gmtime((t1 - t0) / (last_img_idx+1) * (imgs_num - last_img_idx - 1) + time.time() - time.altzone)))
        
    return results

def main():

    # Arguments parser ---
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("datasetPath",help="path of the image dataset")
    parser.add_argument("model",help="a valid CNN model ")
    parser.add_argument("platformPrefix",help="platform prefix used as output filename prefix")
    parser.add_argument("-m","--multithread",help="number of processes used to perform feature extraction",type=int,nargs=1)
    parser.add_argument("-b","--batch-size",help="number of images in a single batch",type=int,nargs=1)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-t","--timing",help="perform time profiling",action="store_true")
    group.add_argument("-f","--features",help="perform features extraction",action="store_true")
    group.required = True;
    
    
    args = parser.parse_args()
    
    datasetPath = args.datasetPath
    datasetName = datasetPath.rsplit('/')[-1]
    if len(datasetName) == 0:
        datasetName = datasetPath.rsplit('/')[-2]
    modelName = args.model
    filenamePrefix = args.platformPrefix

    baseFilename = filenamePrefix + '_' + datasetName + '_' + modelName
    
    # MultiTheading check ---
    multiThread = False
    if args.multithread:
        multiThread = args.multithread[0]
        
    if multiThread != False and multiThread > 1:
        try:
            import multiprocessing
            if multiprocessing.cpu_count() > 1:
                from joblib import Parallel, delayed
            else:
                multiThread = False
        except ImportError:
            multiThread = False
    else:
        multiThread = False
        
    # Batch size check ---
    batch_size = BATCH_SIZE_DEFAULT
    if args.batch_size:
        batch_size = args.batch_size[0]
           
    """
    Timing and features check. Disable MultiThreading
    """ 
    timingEnable = False
    featuresEnable = False
    if args.timing:
        timingEnable = True
        multiThread = False
    if args.features:
        featuresEnable = True

    """
    CNN initialization
    """
    if not fct.checkModel(modelName):
        exit();

    model = cnnmodels.models[modelName]
    modelDescr = model['description']
    modelParams = model['input-params']

    imageSize = modelParams['imageSize']
    
    featuresShape = fct.featureShape(model)

    print('Features shape = ' + str(featuresShape))
    
    featuresExtractor = cnn.BatchExtractor(imageSize, modelDescr)

    """
    Images list
    """
    dataObj = data.cvprw11_setup(datasetPath)
    imgsPath = dataObj.classification_task()[0]
    imgsNum = len(imgsPath)
    
    # Images cycle - Features extraction and timing ---
    timings = imgsNum * [None]
    features = np.empty((imgsNum,) + featuresShape, dtype=np.float32)
    print("Images features extraction")
    t0 = time.time()
    if multiThread != False:
        print("Parallelized extraction with " + str(multiThread) + " processes")
        
        batch_num = np.ceil(imgsNum/float(batch_size)).astype(np.int16)
        results = Parallel(n_jobs=multiThread)(delayed(image_set_extract)(featuresExtractor,
                              imgsPath[np.arange(batch_idx*batch_size,min([(batch_idx+1)*batch_size,imgsNum-1]))],
                              imageSize,
                              batch_idx*batch_size,
                              imgsNum, t0) for batch_idx in range(batch_num))
        for batch_idx,result in enumerate(results):
            idxs = np.arange(batch_idx*batch_size,min([(batch_idx+1)*batch_size,imgsNum-1]))
            if featuresEnable: features[idxs,:,:] = result[0]
            if timingEnable: timings[idxs] = result[1]/batch_size
        
    else:
        if timingEnable:
            # single image load and extract ---
            for imgIdx, imgPath in enumerate(imgsPath):
                features[imgIdx], timings[imgIdx] = \
                    imageExtract(featuresExtractor,imgPath,imageSize,imgIdx,imgsNum,t0)
        else:
            # multiple images load and extract ---
            batch_num = np.ceil(imgsNum/float(batch_size)).astype(np.int16)
            for batch_idx in range(batch_num):
                idxs = np.arange(batch_idx*batch_size,min([(batch_idx+1)*batch_size,imgsNum-1]))
                result = image_set_extract(featuresExtractor,
                                  imgsPath[idxs],
                                  imageSize,
                                  batch_idx*batch_size,
                                  imgsNum, t0)
                features[idxs,:,:] = result[0]
    
    assert features.dtype == np.float32
    assert features.shape == (imgsNum,) + featuresShape
    
    """
    Saving features, data, timings object
    """
    print('\n--------\n')
    print('Total extraction time: {:.2f}s'.format(time.time()-t0))
    
    if timingEnable:
        
        time_mean, time_std = fct.timing_stats(timings)
        
        print('Average feature extraction time per image: {:3.4f}s (+/-{:2.2f}%)'.format(time_mean,time_std/time_mean))
        fct.createDir(TIME_PATH)
        time_filename = os.path.join(TIME_PATH,baseFilename+'.pickle')
        print('Saving time in ' + time_filename)
        fct.save(timings,time_filename)
    if featuresEnable:
        fct.createDir(FEAT_PATH)
        
        data_filename = os.path.join(FEAT_PATH,baseFilename+'.pickle')
        
        if os.path.isfile(data_filename):
            print('Data file already saved. Not overwriting to preserve splits of PCA.')
        else:
            print('Saving data in ' + data_filename)
            # generate splits so to have them saved
            try:
                dataObj.classification_splits
            except AssertionError:
                print('Dataset not suitable for 10 fold cross validation with PubFig83 protocol')
            fct.save(dataObj,data_filename)
        
        features_filename = os.path.join(FEAT_PATH,baseFilename+'.npy')
        print('Saving features in ' + features_filename)
        np.save(features_filename,features)
        
if __name__ == "__main__":
    main();
