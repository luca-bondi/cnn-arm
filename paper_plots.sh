#!/bin/bash

# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD

./atc_model_search_plot.py results/atc/search/pc_pf83aligned-whole_fg11-ht-l2-s1.pickle results/atc/search/rpi_pf83aligned-performance_fg11-ht-l2-s1.pickle -r 87.13

./atc_model_search_plot.py results/atc/search/pc-rpi_fg11-ht-l2-wide.pickle -r 87.13

./atc_cta_plot.py \
	-a results/atc/accuracy/pc_pf83aligned-whole_fg11-ht-l2-opt_pca-1600-3200-6400_qstep-0.0-0.005-0.01-0.03-0.05.pickle \
	-c results/cta/accuracy/pc_pf83aligned-whole_fg11-ht-l2-opt.pickle \
	results/cta/accuracy/pc_pf83aligned-whole_fg11-ht-l3-1.pickle \
	--ref-l2 86.73 --ref-l3 87.13