#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Authors: 
#          Luca Bondi <luca.bondi@polimi.it>
#          Luca Baroffio <luca.baroffio@polimi.it>
#          Matteo Cesana <matteo.cesana@polimi.it>
#          Marco Tagliasacchi <marco.tagliasacchi@polimi.it>
#          Giovani Chiachia <chiachia@ic.unicamp.br>
#          Anderson Rocha <rocha@ic.unicamp.br>
#
# License: BSD
"""
Accuracy plot
"""
import os
import argparse
import functions as fct
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

PLOT_DIR = 'results/atc_vs_cta/'

cta_marker = 'o'
cta_linestyle = '-.'
cta_color_1 = np.array((0.2,0.7,0.2))
cta_color_2 = np.array((0,1,0))

atc_marker = 'o'
atc_linestyle = '-'
atc_color_1 = np.array((0.3,0.0,0.0))
atc_color_2 = np.array((1,0.4,0.4))

markersize = 10
linewidth = 2

legend_font_size = 18
font_size = 22
annotation_font_size = 22

# Plots to be generated ---
plot_accuracy_rate_pca_only = False
plot_accuracy_rate_quant_only = False
plot_accuracy_rate_pca_quant = True
plot_rate_distortion = False
plot_snr_rate = False
plot_accuracy_snr = False
plot_accuracy_distortion = False

def main():
    
    # Matplotlib options ---
    font = {
            'family' : 'sans',
            'weight' : 'normal',
            'size'   : font_size}
    matplotlib.rc('font', **font)
    
    plt.rcParams['xtick.major.pad']='10'
    plt.rcParams['ytick.major.pad']='7'
    
    # Arguments parser ---
    parser = argparse.ArgumentParser()
    # Positional arguments
    parser.add_argument("-a","--atc",help="path of the atc accuracy results",required = True, nargs = 1)
    parser.add_argument("-c","--cta",help="path of the cta accuracy results",required = False, nargs = "+")
    parser.add_argument("-p","--pca",help="pca with the provided number of components",type=int,nargs="+")
    parser.add_argument("-q","--qstep",help="quantization steps",type=float,nargs="+")
    parser.add_argument("--ref-l2",help="reference accuracy fg11-ht-l2-opt",type=float,nargs=1)
    parser.add_argument("--ref-l3",help="reference accuracy fg11-ht-l3-1",type=float,nargs=1)
    args = parser.parse_args()
    
    try:
        accSaved = fct.load(args.atc[0])
    except IOError:
        exit(args.atc[0] + ' not found')
    
    cta_data_list = []
    cta_models = []
    if args.cta:
        for cta_path in args.cta:
            try:
                cta_data_list += [fct.load(cta_path),]
                cta_path,_ = os.path.splitext(cta_path)
                if cta_path.endswith('.'): cta_path = cta_path[:-1]
                cta_models += [cta_path.split("_")[2],]
            except IOError:
                exit(cta_path + ' not found')
    
    filename_prefix = os.path.basename(args.atc[0])
    filename_prefix, _ = os.path.splitext(filename_prefix)
    if filename_prefix.endswith('.'): filename_prefix = filename_prefix[:-1]
    
    platform_name, dataset_name, model_name = \
        filename_prefix.split('_')[:3]
    
    filename_prefix = platform_name + '_' + dataset_name + '_' + model_name

    print('Platform: ' + platform_name)
    print('Dataset: ' + dataset_name)
    print('Model: ' + model_name)
    
    # Load data from saved accuracy file
    atc_acc = accSaved['accuracy']
    atc_pca = accSaved['pca']
    atc_size = accSaved['rate']
    atc_dist = accSaved['snr']
    atc_qstep = accSaved['qstep']
    
    pca_values = np.unique(atc_pca)
    qstep_values = np.unique(atc_qstep)
    
    pca_mask = np.ones(atc_acc.shape, np.bool)
    if args.pca:
        pca_values = np.intersect1d(pca_values,np.array(args.pca,dtype=np.int16)).astype(np.int16)
        for idx, val in enumerate(atc_pca):
            if val not in pca_values:
                pca_mask[idx] = False
                
    qstep_mask = np.ones(atc_acc.shape, np.bool)
    if args.qstep:
        qstep_values = np.intersect1d(qstep_values,np.array(args.qstep,dtype=np.float32)).astype(np.float32)
        for idx, val in enumerate(atc_qstep):
            if val not in qstep_values:
                qstep_mask[idx] = False
    
    filename_prefix += '_pca-'+'-'.join(map(str,pca_values))
    filename_prefix += '_qstep-'+'-'.join(map(str,qstep_values))
    
    print('pca: ' + str(pca_values))
    print('qstep: ' + str(qstep_values))
    
    fct.createDir(PLOT_DIR)
    
    #===========================================================================
    # Prepare pca and levels arrays ---
    #===========================================================================
    
    # Reference accuracy (no pca, no q step). Input overwrites data
    ref_acc = atc_acc[np.bitwise_and(atc_pca == 0, atc_qstep == 0)]
    if args.ref_l2:
        ref_acc_l2 = np.float32(args.ref_l2[0])
    else:
        ref_acc_l2 = None
    if args.ref_l3:
        ref_acc_l3 = np.float32(args.ref_l3[0])
    else:
        ref_acc_l3 = None
     
    #===========================================================================
    # Acc vs rate - KLT only ---
    #===========================================================================
    if plot_accuracy_rate_pca_only:
        if atc_pca.max() > 0 and atc_qstep.min() == 0:
            plt.figure()
            ax = plt.subplot(111)
            
            # No KLT line
            if ref_acc.size: plt.axhline(ref_acc,linestyle='--',color = 'k',label=model_name)
            idxs = np.bitwise_and(np.bitwise_and(atc_qstep == 0, atc_pca > 0),pca_mask)
            plt.plot(atc_size[idxs][::-1],atc_acc[idxs][::-1],marker=atc_marker,linestyle=atc_linestyle,\
                     label=model_name + ' + KLT')
            
            #JPEG
            for cta_data, cta_model in zip(cta_data_list,cta_models):    
                plt.plot(cta_data['size'],cta_data['accuracy'],marker=cta_marker,linestyle=cta_linestyle,label='JPEG + ' + cta_model)
            
            plt.xlabel('Rate [bit]')
            plt.ylabel('Accuracy [%]')
            
            #plt.title('KLT components: ' + ', '.join(map(str,pca_values[pca_values > 0])))
             
            ax.legend(loc='lower right',fontsize=legend_font_size)
            plt.grid(True)
            
            #plt.title('CNN model: ' + model_name)
            
            figure_filepath = os.path.join(PLOT_DIR,filename_prefix+'_pca.pdf')
            print('KLT plot saved in ' + figure_filepath)
            plt.savefig(figure_filepath)
    
    #==========================================================================
    # Acc vs rate - Quantization only ---
    #==========================================================================
    if plot_accuracy_rate_quant_only:
        if atc_qstep.max() > 0 and atc_pca.min() == 0:
            plt.figure()
            ax = plt.subplot(111)
            
            # No Q line
            if ref_acc.size: plt.axhline(ref_acc,linestyle='--',color = 'k',label=model_name)
            idxs = np.bitwise_and(np.bitwise_and(atc_pca == 0,atc_qstep > 0),qstep_mask)
            plt.plot(atc_size[idxs][::-1],atc_acc[idxs][::-1],marker=atc_marker,linestyle=atc_linestyle,\
                     label=model_name + ' + Q' )
                
            #JPEG    
            for cta_data, cta_model in zip(cta_data_list,cta_models):    
                plt.plot(cta_data['size'],cta_data['accuracy'],marker=cta_marker,linestyle=cta_linestyle,label='JPEG + ' + cta_model)
            
            plt.xlabel('Rate [bit]')
            plt.ylabel('Accuracy [%]')
             
            ax.legend(loc='lower right',fontsize=legend_font_size)
            plt.grid(True)
            
            #plt.title('Quantization step: ' + ', '.join(map(str,qstep_values[qstep_values > 0][::-1])))
            
            figure_filepath = os.path.join(PLOT_DIR,filename_prefix+'_quant.pdf')
            print('Quantization plot saved in ' + figure_filepath)
            plt.savefig(figure_filepath)
    
    #==========================================================================
    # Acc vs rate - KLT and quantization ---
    #==========================================================================
    if plot_accuracy_rate_pca_quant:
        if atc_qstep.max() > 0 and atc_pca.max() > 0:
            
            fig = plt.figure(figsize=(10,6),dpi=72)
            fig.subplots_adjust(left=0.10,bottom=0.14,right=0.94,top=0.93)
            ax = plt.subplot(111)
            
            for par_idx, pca in enumerate(pca_values[::-1]):
                if pca > 0:
                    #idxs = np.bitwise_and(atc_pca == pca,atc_qstep == 0)
                    #if idxs.sum(): plt.axhline(atc_acc[idxs],linestyle='--')
                    #if idxs.sum(): plt.axhline(atc_acc[idxs],linestyle='--', label='ATC: ' + model_name + ' + KLT ' + str(pca))
                    idxs = np.bitwise_and(np.bitwise_and(atc_pca == pca,atc_qstep > 0),qstep_mask)
                    #col = colors.rgb2hex(tuple(atc_base_color) + ((par_idx+1.)/len(pca_values),))
                    col = atc_color_1+(atc_color_2-atc_color_1)/(len(pca_values)-1)*par_idx
                    plt.plot(atc_size[idxs],atc_acc[idxs],color=col,marker=atc_marker,markersize=markersize,linewidth=linewidth,linestyle=atc_linestyle,label='ATC: ' + model_name + ' + KLT ' + str(pca) + ' + Q' )
        
            #JPEG
            for par_idx, cta_data, cta_model in zip(range(len(cta_data_list)),cta_data_list,cta_models):
                #col = ((par_idx+1.)/len(cta_models),) + tuple(cta_base_color)
                col = cta_color_1+(cta_color_2-cta_color_1)/(len(cta_models)-1)*par_idx
                plt.plot(cta_data['size'],cta_data['accuracy'],color=col,marker=cta_marker,markersize=markersize,linewidth=linewidth,linestyle=cta_linestyle,label='CTA: JPEG + ' + cta_model)
            
            _, x_max = plt.xlim()
            
            # No KLT nor Q line
            #if ref_acc.size: plt.axhline(ref_acc,linestyle='--',color = 'k',label=model_name)
            #if ref_acc.size: plt.axhline(ref_acc,linestyle='--',color = 'k',label=model_name+' (no KLT, float32)')
            if ref_acc_l2: 
                plt.axhline(ref_acc_l2,linestyle='--',color = 'k',linewidth=linewidth)
                ax.annotate('l2-opt', xy=(0, ref_acc_l2), xytext=(-x_max*0.08, ref_acc_l2),
                            arrowprops=dict(frac=0.3,width=1,facecolor='black', shrink=0.1, headwidth=7),
                            size=annotation_font_size,
                            ha = 'center', va = 'top', rotation='vertical'
                )
            if ref_acc_l3: 
                plt.axhline(ref_acc_l3,linestyle='--',color = 'k',linewidth=linewidth)
                ax.annotate('l3-1', xy=(0, ref_acc_l3), xytext=(-x_max*0.08, ref_acc_l3),
                            size=annotation_font_size,
                arrowprops=dict(frac=0.3,width=1,facecolor='black', shrink=0.1, headwidth=7),
                ha = 'center', va = 'bottom', rotation='vertical'
                )
            
                    
            plt.xlabel('Rate [bit]')
            plt.ylabel('Accuracy [%]')
             
            ax.legend(loc='lower right',fontsize=legend_font_size)
            plt.grid(True)
            
            #plt.title('Quantization step: ' + ', '.join(map(str,qstep_values[qstep_values > 0][::-1])))
            
            figure_filepath = os.path.join(PLOT_DIR,filename_prefix+'_pca-quant.pdf')
            print('KLT + quantization plot saved in ' + figure_filepath)
            plt.savefig(figure_filepath)
        
    
    #===========================================================================
    # Rate vs Distortion ---
    #===========================================================================
    if plot_rate_distortion:
        if atc_dist is not None and atc_qstep.max() > 0:
            plt.figure()
            for pca in pca_values[::-1]:
                idxs = np.bitwise_and(np.bitwise_and(atc_pca == pca,atc_qstep > 0),qstep_mask)
                if pca == 0: 
                    plt.plot(10**(-atc_dist[idxs,0]/20),atc_size[idxs],'-o',label=model_name + ' + Q')
                else:
                    plt.plot(10**(-atc_dist[idxs,0]/20),atc_size[idxs],'-o',label=model_name + 'KLT ' + str(pca)+ ' + Q')
            plt.axvline(1,linestyle='--',color='k')
            plt.xlabel('Distortion')
            plt.ylabel('Rate [bit]')
            plt.legend(fontsize=legend_font_size)
            
            auto_axis = list(plt.axis())
            auto_axis[1] = 1.1
            plt.axis(auto_axis)
            
            #plt.title('Quantization step: ' + ', '.join(map(str,qstep_values[qstep_values > 0][::-1])))
            
            figure_filepath = os.path.join(PLOT_DIR,filename_prefix+'_rate-dist.pdf')
            print('KLT + quantization plot saved in ' + figure_filepath)
            plt.savefig(figure_filepath)
        
    #=======================================================================
    # SNR vs rate ---
    #=======================================================================
    if plot_snr_rate:
        if atc_dist is not None and atc_qstep.max() > 0:
            plt.figure()
            for pca in pca_values:
                idxs = np.bitwise_and(np.bitwise_and(atc_pca == pca,atc_qstep > 0),qstep_mask)
                if pca == 0: 
                    plt.plot(atc_size[idxs],atc_dist[idxs,0],'-o',label=model_name+ ' + Q')
                else:
                    plt.plot(atc_size[idxs],atc_dist[idxs,0],'-o',label=model_name+' + KLT ' + str(pca)+ ' + Q')
            plt.ylabel('SNR [dB]')
            plt.xlabel('Rate [bit]')
            plt.legend(loc='lower right',fontsize=legend_font_size)
            
            #plt.title('Quantization step: ' + ', '.join(map(str,qstep_values[qstep_values > 0][::-1])))
            
            figure_filepath = os.path.join(PLOT_DIR,filename_prefix+'_snr-rate.pdf')
            print('KLT + quantization plot saved in ' + figure_filepath)
            plt.savefig(figure_filepath)
        
    #=======================================================================
    # Accuracy vs SNR ---
    #=======================================================================
    if plot_accuracy_snr:
        if atc_dist is not None and atc_qstep.max() > 0:
            plt.figure()
            color_idx = 0
            if ref_acc.size: plt.axhline(ref_acc,linestyle='--',color='k',label=model_name)
            for pca in pca_values[::-1]:
                idxs = np.bitwise_and(np.bitwise_and(atc_pca == pca,atc_qstep > 0),qstep_mask)
                if pca == 0: 
                    plt.plot(atc_dist[idxs,0],atc_acc[idxs],'-o',label=model_name+ ' + Q')
                else:
                    idxs_hline = np.bitwise_and(atc_pca == pca,atc_qstep == 0)
                    if idxs_hline.sum(): plt.axhline(atc_acc[idxs_hline],linestyle='--', label=model_name + ' + KLT ' + str(pca))
                    plt.plot(atc_dist[idxs,0],atc_acc[idxs],'-o',label=model_name + ' + KLT ' + str(pca) + ' + Q')
                color_idx += 1
            plt.xlabel('SNR [dB]')
            plt.ylabel('Accuracy [%]')
            plt.legend(loc='lower right',fontsize=legend_font_size)
            
            #plt.title('Quantization step: ' + ', '.join(map(str,qstep_values[qstep_values > 0][::-1])))
            
            figure_filepath = os.path.join(PLOT_DIR,filename_prefix+'_acc-snr.pdf')
            print('KLT + quantization plot saved in ' + figure_filepath)
            plt.savefig(figure_filepath)
        
    #=======================================================================
    # Accuracy vs Distortion ---
    #=======================================================================
    if plot_accuracy_distortion:
        if atc_dist is not None and atc_qstep.max() > 0:
            plt.figure()
            color_idx = 0
            if ref_acc.size: plt.axhline(ref_acc,linestyle='--',color='k',label=model_name)
            for pca in pca_values[::-1]:
                idxs = np.bitwise_and(np.bitwise_and(atc_pca == pca,atc_qstep > 0),qstep_mask)
                if pca == 0: 
                    plt.plot(10**(-atc_dist[idxs,0]/20),atc_acc[idxs],'-o',label=model_name + ' + Q')
                else:
                    idxs_hline = np.bitwise_and(atc_pca == pca,atc_qstep == 0)
                    if idxs_hline.sum(): plt.axhline(atc_acc[idxs_hline],linestyle='--', label=model_name + ' + KLT ' + str(pca))
                    plt.plot(10**(-atc_dist[idxs,0]/20),atc_acc[idxs],'-o',label=model_name + ' + KLT ' + str(pca) + ' + Q')
                color_idx += 1
            plt.axvline(1,linestyle='--',color='k')
            plt.xlabel('Distortion ')
            plt.ylabel('Accuracy [%]')
            plt.legend(fontsize=legend_font_size)
            
            auto_axis = list(plt.axis())
            auto_axis[1] = 1.1
            plt.axis(auto_axis)
            
            #plt.title('Quantization step: ' + ', '.join(map(str,qstep_values[qstep_values > 0][::-1])))
            
            figure_filepath = os.path.join(PLOT_DIR,filename_prefix+'_acc-dist.pdf')
            print('KLT + quantization plot saved in ' + figure_filepath)
            plt.savefig(figure_filepath)
    
    # Show plots
    #plt.show()

if __name__ == "__main__":
    main();